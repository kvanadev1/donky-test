//
//  com_dynmark_donky_demoAppDelegate.m
//  Donky
//
//  Created by Technical on 27/01/2014.
//  Copyright (c) 2014 Dynmark International Limited. All rights reserved.
//

#import "AppDelegate.h"

#import "DKUser.h"
#import "DKDonkyUI.h"
#import "DKDonkyClient.h"
#import "ExampleAppViewController.h"
#import "DKRegistrationViewController.h"

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // Initialise application window
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    self.window.backgroundColor = [UIColor whiteColor];
    [self.window makeKeyAndVisible];
    
    // Define your Donky Messaging API key and config dictionary
    NSString *apiKey = @"PTERU3XjH4wSC+dNZZ9opyPHAWyXokhwDDeHyd43wW2RE5mKuOH9Q4+I0vfoHfTWA4mriwmVaJZGy5Z+zU3BQ";
    NSDictionary *config = [NSDictionary dictionaryWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"config" ofType:@"plist"]];
    
    UIStoryboard *sb;
    DKRegistrationViewController *registrationViewController;
    // Setting example app registartion view controller as initial View Controller
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        // The device is an iPad.
        registrationViewController = [[UIStoryboard storyboardWithName:@"Main_iPad" bundle:nil] instantiateInitialViewController];
        sb = [UIStoryboard storyboardWithName:@"Main_iPad" bundle:nil];
    }
    else {
        // The device is an iPhone or iPod touch.
        registrationViewController = [[UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil] instantiateInitialViewController];
        sb = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
    }
    
    self.window.rootViewController = registrationViewController;
    registrationViewController.delegate = self;
    [registrationViewController showLoadingView];
    
    // Initialise Donky SDK in separate thread
    [DKDonkyClient initializeWithAPIKey:apiKey configuration:config completion:^(NSError *error) {
        // Check if user is registered
        if ([[DKDonkyClient sharedInstance] isRegistered]) {
            // show main example app view controller.
            ExampleAppViewController * vc = (ExampleAppViewController *)[sb instantiateViewControllerWithIdentifier:@"mainViewController"];
            self.window.rootViewController = vc;
            // Check if user is logged in
            if (![[DKDonkyClient sharedInstance] isLoggedIn]) {
                [[DKDonkyClient sharedInstance] login:^(NSError *error) {
                    if (error == nil) {
                        // User logged in.
                    } else {
                        // Error when logging in
                        [[[UIAlertView alloc] initWithTitle:@"Error" message:[error localizedDescription] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
                        [registrationViewController hideLoadingView];
                    }
                }];
            } else {
                // User logged in. Showing main view controller
                ExampleAppViewController * vc = (ExampleAppViewController *)[sb instantiateViewControllerWithIdentifier:@"mainViewController"];
                self.window.rootViewController = vc;
            }
        } else {
            // User not registered.
            [registrationViewController hideLoadingView];
        }
   }];
    
    return YES;
}

/**
 DonkyRegistrationDelegate callback method. Registered to Donky Network
 */
- (void)registrationViewControllerDidRegister:(DKRegistrationViewController *)registerViewController
{
    UIStoryboard *sb;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        // The device is an iPad.
        sb = [UIStoryboard storyboardWithName:@"Main_iPad" bundle:nil];
    }
    else {
        // The device is an iPhone or iPod touch.
        sb = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
    }
    ExampleAppViewController * vc = (ExampleAppViewController *)[sb instantiateViewControllerWithIdentifier:@"mainViewController"];
    self.window.rootViewController = vc;
}

/**
 DonkyRegistrationDelegate callback method. Logged into Donky Network
 */
- (void)registrationViewControllerDidLogin:(DKRegistrationViewController *)registerViewController
{
    UIStoryboard *sb;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        // The device is an iPad.
        sb = [UIStoryboard storyboardWithName:@"Main_iPad" bundle:nil];
    }
    else {
        // The device is an iPhone or iPod touch.
        sb = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
    }
    ExampleAppViewController * vc = (ExampleAppViewController *)[sb instantiateViewControllerWithIdentifier:@"mainViewController"];
    self.window.rootViewController = vc;
}

- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
{
    // When you send this message, the device initiates the registration process with Apple Push Service. If it
    // succeeds, the app delegate receives a device token in
    // the application:didRegisterForRemoteNotificationsWithDeviceToken:method;
    [[DKDonkyClient sharedInstance].apnsController registerAPNS:deviceToken];
}

- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error
{
    // if registration doesn’t
    // succeed, the delegate is informed via
    // the application:didFailToRegisterForRemoteNotificationsWithError: method.
}

- (void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification
{
    // Local notifications are similar to remote push notifications, but differ in that they are scheduled, displayed,
    // and received entirely on the same device. An app can create and schedule a local notification, and the
    // operating system then delivers it at the schedule date and time. If it delivers it when the app is not active in
    // the foreground, it displays an alert, badges the app icon, or plays a sound—whatever is specified in
    // the UILocalNotification object. If the app is running in the foreground, there is no alert, badging, or sound;
    //instead, the application:didReceiveLocalNotification: method is called if the delegate implements it.
    [[DKDonkyUI sharedInstance].notificationPresentationController presentLocalNotification:notification];
}


//- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
//{
//    // If the app is running and receives a remote notification, the app calls this method to process the notification.
//    if (application.applicationState != UIApplicationStateActive) {
//        [[DKDonkyUI sharedInstance].notificationPresentationController handleRemoteNotification:userInfo];
//    } else {
//        [[DKDonkyClient sharedInstance].apnsController handleNotification:userInfo];
//    }
//}

//- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult result))completionHandler
//{
//    // Tells the app that a push notification arrived that indicates there is data to be fetched.
//    if (application.applicationState != UIApplicationStateActive) {
//        [[DKDonkyUI sharedInstance].notificationPresentationController handleRemoteNotification:userInfo];
//    } else {
//        [[DKDonkyClient sharedInstance].apnsController handleNotification:userInfo];
//    }
//    
//    completionHandler(UIBackgroundFetchResultNoData);
//}

@end