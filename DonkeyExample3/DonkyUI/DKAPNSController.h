//
//  DKAPNSController.h
//  DonkySDK
//
//  Created by David Taylor on 21/11/2013.
//  Copyright (c) 2013 Compsoft. All rights reserved.
//

#import <Foundation/Foundation.h>

/*! 
 @abstract A protocol that provides functionality for handing back remote notifications that are not Donky related to the integrating app
 */
@protocol DKAPNSControllerDelegate <NSObject>

/*!  
 @abstract A mechanism for remote notifications to pass back remote notification data that is not Donky related
 
 @param userInfo The remote notification data
 */
- (void)apnsControllerDidPassBackNotification:(NSDictionary *)userInfo;

@end

/*! 
 @abstract Responsible for the registration and handling of remote notifications
 */
@interface DKAPNSController : NSObject

/*! 
 @abstract An object that accepts notifications that are not used by Donky SDK
 */
@property (weak, nonatomic) id<DKAPNSControllerDelegate> delegate;

/*!  
 @abstract Registers the device with the Donky Messaging Network
 
 @param token The device token received after registering for remote notifications
 */
- (void)registerAPNS:(NSData *)token;

/*!  
 @abstract Filters out and acts upon Donky related remote notifications
 
 @param userInfo The remote notification to handle
 */
- (void)handleNotification:(NSDictionary *)userInfo;

@end
