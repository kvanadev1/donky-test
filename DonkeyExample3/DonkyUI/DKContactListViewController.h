//
//  DKContactListViewController.h
//  DonkySDK
//
//  Created by David Taylor on 10/12/2013.
//  Copyright (c) 2013 Compsoft. All rights reserved.
//

#import "DKBaseTableViewController.h"
#import "DKBaseViewController.h"
#import "DKDonkyClient.h"
#import <MessageUI/MessageUI.h>

@class DKContactListViewController, DKContact;

/*! 
 @abstract Events raised by DKContactListViewController
 */
@protocol DKContactListViewControllerDelegate <NSObject>

@optional
/*!  
 @abstract Called when the user taps on a contact
 
 @param contactListViewController The DKContactListViewController that raised the event
 @param contact                   The contact tapped
 */
- (void)contactListViewController:(DKContactListViewController *)contactListViewController didSelectContact:(DKContact *)contact;

/*!  
 @abstract Called when the user attempts to start a new chat with a contact
 
 @param contactListViewController The DKContactListViewController that raised the event
 @param contact                   The contact tapped
 */
- (void)contactListViewController:(DKContactListViewController *)contactListViewController didSelectNewChatForContact:(DKContact *)contact;

/*!  
 @abstract Called when the user attempts to start a new group chat with a contact
 
 @param contactListViewController The DKContactListViewController that raised the event
 @param contact                   The contact tapped
 */
- (void)contactListViewController:(DKContactListViewController *)contactListViewController didSelectNewGroupChatForContact:(DKContact *)contact;

/*!  
 @abstract Called when the user taps the add contact button
 
 @param contactListViewController The DKContactListViewController that raised the event
 @param contact                   The contact tapped
 */
- (void)didSelectAddContactForContactListViewController:(DKContactListViewController *)contactListViewController;

/*!  
 @abstract Called when the user taps the invite contact button
 
 @param contactListViewController The DKContactListViewController that raised the event
 @param contact                   The contact tapped
 */
- (void)didSelectInviteContactForContactListViewController:(DKContactListViewController *)contactListViewController;

/*!  
 @abstract Called when the user taps the find contacts button
 
 @param contactListViewController The DKContactListViewController that raised the event
 @param contact                   The contact tapped
 */
- (void)didSelectFindContactsForContactListViewController:(DKContactListViewController *)contactListViewController;

@end

/*! 
 @abstract Displays a list of contacts and allows the user to add, invite or find new contacts as well as opening a conversation thread for a specific contact
 */
@interface DKContactListViewController : DKBaseViewController <UITableViewDataSource, UITableViewDelegate, UIActionSheetDelegate, UIAlertViewDelegate, UISearchDisplayDelegate, DKContactControllerDelegate, MFMessageComposeViewControllerDelegate, MFMailComposeViewControllerDelegate>

/*! 
 @abstract Overrides events raised by DKContactListViewController
 */
@property (weak, nonatomic) id <DKContactListViewControllerDelegate> delegate;

/*! 
 @abstract Controls whether the contact list hides the standard contacts
 */
@property (nonatomic) BOOL hideStandardContacts;

/*! 
 @abstract Controls whether the find, invite and add contacts buttons are visible
 */
@property (nonatomic) BOOL hideAddContactsButtons;

@end
