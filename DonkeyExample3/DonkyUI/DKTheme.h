//
//  DKTheme.h
//  DonkySDK
//
//  Created by David Taylor on 02/12/2013.
//  Copyright (c) 2013 Compsoft. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum {
    DKThemeBarStyleDefault,
    DKThemeBarStyleGlossy,
    DKThemeBarStyleFlat,
}DKThemeBarStyle;

// NavBar
extern NSString * const DKColorNavBarBackground;
extern NSString * const DKColorNavigationBarTitleColor;
extern NSString * const DKColorNavigationBarTintColor;

// TabBar
extern NSString * const DKColorTabBarBackground;
extern NSString * const DKColorTabBarIconTint;
extern NSString * const DKColorTabBarSelectedIconTint;
extern NSString * const DKColorTabBarTitle;

// Rich Message List
extern NSString * const DKColorListCellScrollViewContentBackground;
extern NSString * const DKColorCellBackground;
extern NSString * const DKColorCellBackgroundTapped;
extern NSString * const DKColorCellDeleteButtonBackground;
extern NSString * const DKColorCellDeleteButtonText;
extern NSString * const DKColorCellForwardButtonBackground;
extern NSString * const DKColorCellForwardButtonText;
extern NSString * const DKColorSearchBar;
extern NSString * const DKColorSearchBarCancelText;
extern NSString * const DKColorRichMessageListCellDateReceivedLabel;
extern NSString * const DKColorRichMessageListCellDescriptionLabel;
extern NSString * const DKColorRichMessageListCellNewLabel;
extern NSString * const DKColorRichMessageListCellForwardButton;
extern NSString * const DKColorRichMessageListCellDeleteButton;
extern NSString * const DKColorRichMessageListCellBackgroundFadeView;
extern NSString * const DKColorRichMessageListCellIsReadViewBackground;
extern NSString * const DKColorRichMessageListCellShapeLayerStrokeColor;
extern NSString * const DKColorRichMessageListCellSeperatorColor;

// Rich Message View
extern NSString * const DKColorRichMessageReplyViewBackground;
extern NSString * const DKColorRichMessageReplyViewForwardedByLabel;
extern NSString * const DKColorRichMessageReplyViewDisplayNameLabel;
extern NSString * const DKColorRichMessageReplyViewMessageLabel;
extern NSString * const DKColorRichMessageReplyViewCloseButtonBackground;
extern NSString * const DKColorRichMessageReplyViewReplyButtonBackground;
extern NSString * const DKColorRichMessageViewBackgroundView;
extern NSString * const DKColorRichMessageViewChatReplyView;
extern NSString * const DKColorRichMessageViewRichMessageReplyView;

// Chat Messaage Inbox
extern NSString * const DKColorChatInboxCellDeleteButton;
extern NSString * const DKColorChatInboxCellContactLabel;
extern NSString * const DKColorChatInboxCellDefaultBlackLabel;
extern NSString * const DKColorChatInboxCellNewLabel;
extern NSString * const DKColorChatInboxCellScrollViewBackground;
extern NSString * const DKColorChatInboxCellisReadView;
extern NSString * const DKColorChatInboxCellContactAvatarImageViewBorder;
extern NSString * const DKColorChatInboxCellSeparatorColor;

// Chat Message View
extern NSString * const DKColorDKMessageImageCellBackground;
extern NSString * const DKColorChatReplyViewSendButtonTitleColor;
extern NSString * const DKColorChatViewBackground;
extern NSString * const DKColorChatViewChatReplyViewBackground;
extern NSString * const DKColorContactChatViewTextView;
extern NSString * const DKColorContactChatViewDisplayNameText;
extern NSString * const DKColorContactChatViewDateText;
extern NSString * const DKColorUserChatCellUserChatViewBackground;
extern NSString * const DKColorUserChatCellContactChatViewBackground;
extern NSString * const DKColorUserChatViewTextViewText;
extern NSString * const DKColorUserChatViewStateText;
extern NSString * const DKColorUserChatViewDateText;
extern NSString * const DKColorChatLeftCroupCellContactLeftLabel;
extern NSString * const DKColorChatJoinGroupCellContactJoinedLabel;
extern NSString * const DKColorChatTypeCellChatTypeLabel;
extern NSString * const DKColorChatTypeCellShapeLayerStrokeColor;

// Contacts
extern NSString * const DKColorContactListSectionHeaderViewLabel;
extern NSString * const DKColorPopupViewButtonBackgroundHighlighted;
extern NSString * const DKColorPopupViewButtonBackgroundDisabled;
extern NSString * const DKColorPopupViewLineLayerStrokeColor;
extern NSString * const DKColorAddContactViewTextFieldBackgroundViewBorder;
extern NSString * const DKColorAddContactViewCompletedUserIdLabelTextColorSuccess;
extern NSString * const DKColorAddContactViewCompletedUserIdLabelTextColorFailure;
extern NSString * const DKColorAddContactViewDefaultLabel;
extern NSString * const DKColorFindContactsViewDefaultLabel;
extern NSString * const DKColorFindContactsViewDefaultPopoverViewBackground;
extern NSString * const DKColorFindContactsViewBackground;
extern NSString * const DKColorFindContactsViewCountAttributedString;
extern NSString * const DKColorContactListCellDisplayNameLabelLightColor;
extern NSString * const DKColorContactListCellDisplayNameLabelDarkColor;
extern NSString * const DKColorContactListCelldonkyContactBackgroundView;
extern NSString * const DKColorContactListCellSelectedViewBackground;
extern NSString * const DKColorContactListBackground;
extern NSString * const DKColorContactListViewBackground;
extern NSString * const DKColorContactListViewTableViewSectionIndex;

// Local notification banner
extern NSString * const DKColorNotificationBannerViewDisplayNameLabel;
extern NSString * const DKColorNotificationBannerViewMessageLabel;
extern NSString * const DKColorNotificationBannerViewButton;
extern NSString * const DKColorNotificationBannerViewNowLabel;
extern NSString * const DKColorNotificationBannerViewAvatarImageViewBorder;

// Recipient List
extern NSString * const DKColorRecipientListViewToLabel;
extern NSString * const DKColorRecipientListViewWarningBannerLabelText;
extern NSString * const DKColorRecipientListViewWarningBannerLabelBackground;
extern NSString * const DKColorRecipientListViewContactViewBackground;
extern NSString * const DKColorRecipientListViewTextFieldText;
extern NSString * const DKColorRecipientListViewListLabel;
extern NSString * const DKColorRecipientListViewNextContactListAttributedText;
extern NSString * const DKColorRecipientListContactViewBackground;
extern NSString * const DKColorRecipientListContactViewNameLabel;
extern NSString * const DKColorChatViewRecipientListBackground;
extern NSString * const DKColorForwardToContactCellDisplayNameLabel;
extern NSString * const DKColorForwardToContactCellDisplayNameLabelLight;
extern NSString * const DKColorForwardToContactCellDisplayNameLabelDark;
extern NSString * const DKColorForwardToContactBackgroundView;
extern NSString * const DKColorForwardToContactCellStandardContactBackgroundView;
extern NSString * const DKColorForwardToContactCellStandardContactBackgroundViewSelected;
extern NSString * const DKColorRichMessageViewRecipientListView;

// Avatar Resizing View
extern NSString * const DKColorScalingImageViewBackgroundView;
extern NSString * const DKColorScalingImageViewContainerViewBackground;
extern NSString * const DKColorScalingImageViewHelpInfoLabel;

//// Settings View
extern NSString * const DKColorPleaseWaitViewMessageLabel;
extern NSString * const DKColorSettingsCellBackground;
extern NSString * const DKColorSettingsCellSelectedBackground;
extern NSString * const DKColorSettingsCellSeparator;
extern NSString * const DKColorSettingsCellTitle;
extern NSString * const DKColorSettingsCellSubtitle;
extern NSString * const DKColorSettingsCellPushDisabledMessage;
extern NSString * const DKColorSettingsCellResetAvatarButtonBackground;
extern NSString * const DKColorSettingsCellResetAvatarButtonTitle;


// Chat messsage attachment View
extern NSString * const DKColorZoomableImageViewBackgroundView;

// Upload attachment View
extern NSString * const DKColorImageManagementViewBackgroundView;

// Misc
extern NSString * const DKColorBaseTableViewControllerNoItemLabel;
extern NSString * const DKColorChatMessageAttributedText;
extern NSString * const DKColorChatMessageAttributedRedText;
extern NSString * const DKColorSizeToFitDefaultText;
extern NSString * const DKColorNoResultsCellNoResultsLabel;

/*! 
 @abstract Represents the theme of the UI
 */
@interface DKTheme : NSObject

/*! 
 @abstract The family name of all fonts in the UI e.g. "HelveticaNeue"
 */
@property (strong, nonatomic) NSString *fontTypeFace;

/*! 
 @abstract A value between 0 and 1 that sets the size of the UI fonts relative to the default size
 */
@property (nonatomic) CGFloat relativeFontSize;

/*! 
 @abstract The tab bar style; default, glossy or flat
 */
@property (nonatomic) DKThemeBarStyle tabBarStyle;

/*! 
 @abstract The nav bar style; default, glossy or flat
 */
@property (nonatomic) DKThemeBarStyle navBarStyle;

/*! 
 @abstract Controls the appearance of the keyboards in the UI
 */
@property (nonatomic) UIKeyboardAppearance keyboardAppearance;

/*! 
 @abstract Sets the status bar style in iOS 7
 */
@property (nonatomic) UIStatusBarStyle statusBarStyle;

/*! 
 @abstract An editiable dictionary of colors whos key are defined above
 */
@property (strong, nonatomic) NSMutableDictionary *colors;

/*!  
 @return A purple theme that is the default Donky theme
 */
+ (DKTheme *)donkyTheme;

/*!  
 @return Nav and tab bars are a glossy black
 */
+ (DKTheme *)glossyBlackTheme;

/*!  
 @return Nav and tab bars are a grey gradient
 */
+ (DKTheme *)greyGradientTheme;

/*!  
 @return Nav and tab bars are a flat red color
 */
+ (DKTheme *)pastelRedTheme;

/*!  
 @return Nav and tab bars are a flat green color
 */
+ (DKTheme *)pastelGreenTheme;

/*!  
 @return Nav and tab bars are a flat blue color
 */
+ (DKTheme *)pastelBlueTheme;

/*!  
 @return Nav and tab bars are a flat yellow color and the tab bar icons are black
 */
+ (DKTheme *)yellowInvertedIconsTheme;

@end
