//
//  DKConversationThread.h
//  DonkySDK
//
//  Created by David Taylor on 04/12/2013.
//  Copyright (c) 2013 Compsoft. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "DKConstants.h"

@class DKChatMessage, DKContact;

/*! 
 @abstract A class that represents a conversation thread
 */
@interface DKConversationThread : NSObject

/*! 
 @abstract The Id of conversation
 */
@property (nonatomic, retain) NSString * conversationId;

/*! 
 @abstract The group Id of the conversation, nil if single chat
 */
@property (nonatomic, retain) NSString * groupId;

/*! 
 @abstract The most recent message text
 */
@property (nonatomic, retain) NSString * mostRecentMessageContent;

/*! 
 @abstract The sent date/time (UTC) of the most recent message
 */
@property (nonatomic, retain) NSDate *mostRecentMessageTimestamp;

/*! 
 @abstract The contact if the conversation is a single chat
 */
@property (nonatomic, retain) DKContact * singleChatContact;

/*! 
 @abstract The most recent chat message if the conversation is a single chat
 */
@property (nonatomic, retain) DKChatMessage * mostRecentChatMessage;

/*! 
 @abstract The most recent chat message if the conversation is a group chat
 */
@property (nonatomic, retain) DKChatMessage * mostRecentGroupChatMessage;

/*! 
 @abstract The number of recieved messages
 */
@property (nonatomic) NSUInteger  numberOfMessages;

/*! 
 @abstract The contacts if the conversation is a group chat
 */
@property (nonatomic, retain) NSArray *participants;

/*! 
 @abstract Whether the conversation is a single or group chat
 */
@property (nonatomic) DKChatType chatType;

/*! 
 @abstract Whether the message is being sent / sent / read / failed to send etc
 */
@property (nonatomic) DKMessageState mostRecentMessageState;

/*! 
 @abstract A message that has been saved against the conversation
 */
@property (copy, nonatomic) NSString *messageInProgress;

/*! 
 @abstract Attachments that have been saved against the conversation
 */
@property (copy, nonatomic) NSArray *assetsInProgress;

/*!  
 @abstract Returns the most recent message
 
 @return mostRecentChatMessage if chat type is DKSingle chat otherwise it returns mostRecentGroupChatMessage
 */
-(DKChatMessage *)mostRecentMessageForChatType;

@end