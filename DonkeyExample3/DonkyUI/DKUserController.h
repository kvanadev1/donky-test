//
//  DKUserController.h
//  DonkySDK
//
//  Created by David Taylor on 09/12/2013.
//  Copyright (c) 2013 Compsoft. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "DKConstants.h"

@class DKUser;

/*!
 A call back after tags have been retrived from the serivce
 
 @param tags  An array of DKTag
 @param error An error when retrieving the tags
 */
typedef void (^DKTagsCompletion) (NSArray *tags, NSError *error);

/*! 
 @abstract Defines a list of methods to retrieve and set information on the user
 */
@interface DKUserController : NSObject

/*!  
 @return The user that is registered
 */
- (DKUser *)user;

/*!  
 @abstract Sets the users avatar
 
 @discussion Scales the image down to 96x96 pixels
 
 @param userAvatar userAvatar The image to set
 @param completion  A block thats call when the service call has been performed
 */
- (void)setUserAvatar:(UIImage *)userAvatar completion:(DKCompletionBlock)completion;

/*!  
 @abstract Sets the users avatar
 
 @discussion Scales the image down to 96x96 pixels
 
 @param filePath The filepath of the image to set
 @param completion  A block thats call when the service call has been performed
 */
- (void)setUserAvatarFromFilePath:(NSString *)filePath completion:(DKCompletionBlock)completion;

/*!  
 @abstract Remove the users avatar asset ID. This updates the service in the background.
 
 @param completion Called on completion
 */
- (void)removeUserAvatarOnCompletion:(DKCompletionBlock)completion;

/*!  
 @return The users avatar
 */
- (UIImage *)userAvatar;

/*!  
 @return The users externalId
 */
- (NSString *)userId;

/*!  
 @abstract Retrieves a list of DKTags from the service
 
 @discussion Tags are not saved on the clients device
 
 @param completion A block that called when the service has retrived
 */
- (void)getUserTags:(DKTagsCompletion)completion;

/*!  
 @abstract Sets the users tag preferences
 
 @discussion Tags are not saved on the clients device
 
 @param tags        An array of DKTags
 @param completion  A block thats call when the service call has been performed
 */
- (void)setUserTags:(NSArray *)tags completion:(DKCompletionBlock)completion;

/*!  
 @return The users display name
 */
- (NSString *)userDisplayName;

/*!  
 @param displayName The users display name
 @param completion  A block thats call when the service call has been performed
 */
- (void)setUserDisplayName:(NSString *)displayName completion:(DKCompletionBlock)completion;

/*!  
 @return The path of the file in the apps bundle that will be played upon receiving a remote notification
 */
- (NSString *)userNotificationSound;

/*!  
 @abstract Sets the sound file that will be played upon receiving a push notification
 
 @param filePath The path of the file in the apps bundle that will be played upon receiving a push notification
 @param completion  A block thats call when the service call has been performed
 */
- (void)setUserNotificationSound:(NSString *)filePath completion:(DKCompletionBlock)completion;

/*!  
 @return YES if the device will vibrate when a push notification is received
 */
- (BOOL)userNotificationVibration;

/*!  
 @param vibration Set YES if the device should vibrate when a push notification is received
 */
- (void)setUserNotificationVibration:(BOOL)vibration;

/*!  
 @discussion Defaults to YES if no user has been set yet
 
 @return Returns whether push notifications will be recieved by Donky Messaging Service
 */
- (BOOL)allowPushNotifications;

/*!  
 @param allow Set to YES to receive push notifications from Donky Messaging Service
 @param completion  A block thats call when the service call has been performed
 */
- (void)setAllowPushNotifications:(BOOL)allow completion:(DKCompletionBlock)completion;

@end
