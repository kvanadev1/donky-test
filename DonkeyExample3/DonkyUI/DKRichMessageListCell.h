//
//  DKRichMessageListCell.h
//  DonkyUI
//
//  Created by David Taylor on 25/11/2013.
//  Copyright (c) 2013 Compsoft. All rights reserved.
//

#import "DKListCell.h"

@class DKRichMessageListCell, DKRichMessage;

extern NSString * const DKRichMessageListCellId;

@interface DKRichMessageListCell : DKListCell <UIScrollViewDelegate>

@property (strong, nonatomic) DKRichMessage *richMessage;

@property (copy, nonatomic) void (^deleteTappedHandler) (DKRichMessageListCell *cell, DKRichMessage *richMessage);
@property (copy, nonatomic) void (^forwardTappedHandler) (DKRichMessageListCell *cell, DKRichMessage *richMessage);

@end
