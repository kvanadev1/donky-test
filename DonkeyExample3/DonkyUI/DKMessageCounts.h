//
//  DKMessageCounts.h
//  DonkySDK
//
//  Created by David Taylor on 27/02/2014.
//  Copyright (c) 2014 Compsoft. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DKMessageCounts : NSObject

- (NSUInteger)richMessageCount;
- (NSUInteger)unreadRichMessageCount;

- (NSUInteger)chatMessageCount;
- (NSUInteger)unreadChatMessageCount;

@end
