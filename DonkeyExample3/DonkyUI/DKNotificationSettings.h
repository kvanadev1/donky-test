//
//  DKNotificationSettings.h
//  DonkySDK
//
//  Created by David Taylor on 27/02/2014.
//  Copyright (c) 2014 Compsoft. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DKNotificationSettings : NSObject

- (NSString *)soundFilePath;
- (BOOL)vibration;

@end
