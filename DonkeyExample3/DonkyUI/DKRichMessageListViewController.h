//
//  DKRichMessageListViewController.h
//  DonkyUI
//
//  Created by David Taylor on 25/11/2013.
//  Copyright (c) 2013 Compsoft. All rights reserved.
//

#import "DKBaseTableViewController.h"
#import "DKDonkyClient.h"
#import "DKLocalNotificationController.h"

@class DKRichMessageListViewController, DKRichMessage;

/*! 
 @abstract Events raised by DKRichMessageListViewController
 */
@protocol DKRichMessageListViewControllerDelegate <NSObject>

@optional
/*!  
 @abstract Called when the user taps a rich message
 
 @param richMessageListViewController The DKRichMessageListViewController that received the tap
 @param richMessage                   The rich message tapped
 */
- (void)richMessageListViewController:(DKRichMessageListViewController *)richMessageListViewController didSelectRichMessage:(DKRichMessage *)richMessage;

/*!  
 @abstract Called when the user taps forward on a rich message
 
 @param richMessageListViewController The DKRichMessageListViewController that received the tap
 @param richMessage                   The rich message to forward
 */
- (void)richMessageListViewController:(DKRichMessageListViewController *)richMessageListViewController didSelectForwardRichMessage:(DKRichMessage *)richMessage;

/*!  
 @abstract Called when the user taps delete on a rich message
 
 @param richMessageListViewController The DKRichMessageListViewController that received the tap
 @param richMessage                   The rich message to be deleted
 */
- (void)richMessageListViewController:(DKRichMessageListViewController *)richMessageListViewController didDeleteRichMessage:(DKRichMessage *)richMessage;

@end

/*! 
 @abstract Displays a list of rich messages
 */
@interface DKRichMessageListViewController : DKBaseTableViewController <UISearchDisplayDelegate, DKRichMessageControllerDelegate, DKLocalNotificationControllerDelegate>

/*! 
 @abstract Overrides events raised by DKRichMessageListViewController
 */
@property (weak, nonatomic) id <DKRichMessageListViewControllerDelegate> delegate;

/*! 
 @abstract Rich messages that are displayed
 */
@property (strong, nonatomic) NSArray *richMessages;

/*!  
 @abstract Highlieghts the table cell that has the specified rich message
 
 @param richMessage The rich message to select
 */
- (void)hightlightCellWithRichMessage:(DKRichMessage *)richMessage;

@end
