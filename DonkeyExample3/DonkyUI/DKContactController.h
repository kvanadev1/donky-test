//
//  DKContactController.h
//  DonkySDK
//
//  Created by David Taylor on 11/12/2013.
//  Copyright (c) 2013 Compsoft. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DKConstants.h"

@class DKContactController, DKContact;

/*!
 @abstract A callback after attempting to add a contact
 
 @param contact The contact added
 @param error   An error on adding the contact
 */
typedef void (^DKAddContactCompletionBlock) (DKContact *contact, NSError *error);

/*!
 @abstract A callback after attempting to find more contacts
 
 @param contact The contacts found
 @param error   An error on finding the contact
 */
typedef void (^DKFindContactsCompletionBlock) (NSArray *contacts, NSError *error);

/*! 
 @abstract Informs delegates of new and updated contacts
 */
@protocol DKContactControllerDelegate <NSObject>

/*!  
 @abstract Passes new, updated and deleted contacts to the delegates
 
 @param addedContacts   New contacts
 @param updatedContacts Updated contacts
 @param removedContacts Deleted contacts
 */
- (void)contactsAdded:(NSArray *)addedContacts updated:(NSArray *)updatedContacts andRemoved:(NSArray *)removedContacts;

@end

/*! 
 @abstract Responsible for the management of user contacts
 */
@interface DKContactController : NSObject

/*!  
 @abstract Returns all contacts matching the passed filter (or all if no filter)
 
 @discussion Results are filtered and ordered by diplayName
 
 @param filter The text to filter contacts by
 @param hideStandardContacts Whether to hide standard contacts or not
 
 @return An array of DKContact
 */
- (NSArray *)allContacts:(NSString *)filter hideStandardContacts:(BOOL)hideStandardContacts;

/*!  
 @abstract Returns all contacts matching the passed filter (or all if no filter)
 
 @param filter The text to filter contacts by
 @param offset The offset if the query
 @param limit  The maximum number of items returned
 @param hideStandardContacts Whether to hide standard contacts or not

 @return An array of DKContact
 */
- (NSArray *)allContacts:(NSString *)filter offset:(NSUInteger)offset limit:(NSUInteger)limit hideStandardContacts:(BOOL)hideStandardContacts;

/*!  
 @abstract Fetch a contact by it's Id
 
 @param contactId The Id
 
 @return The matching contact (or nil)
 */
- (DKContact *)contactById:(NSString *)contactId;

/*!  
 @abstract Returns a list of contacts
 
 @param contactIds An array of external user ids
 
 @return An array of DKContacts
 */

- (NSArray *)contactsByIds:(NSArray *)contactIds;

/*!  
 @abstract Checks the contact exists and adds it you the users contact list
 
 @param contactId   The external user Id of the contact
 @param completion  Called whem the contact has been saved
 */
- (void)addContact:(NSString *)contactId completion:(DKAddContactCompletionBlock)completion;

/*!  
 @abstract Deletes a contact from the users contact list
 
 @param externalUserId The external user Id of the contact to delete
 @param error          On error deleting the contact
 */
- (void)deleteContactWithExternalUserId:(NSString *)externalUserId error:(NSError **)error;

/*!  
 @abstract Finds contacts from the devices address book that are using the Donky Messaging Service
 
 @discussion Uses email addresses and phone numbers to discover contacts that are a member of Donky Messaging Service.
 
 @param phoneNumbers      Phone numbers of contacts to add. An array of NSStrings.
 @param emailAddresses    Email addresses of contacts to add. An array of NSStrings.
 @param completion        Called when all found contacts have been saved
 @param saveAutomatically Whether returned contacts are saved automatically to the contact list. If NO then -addFoundContacts: will need to be called seperatly with the contacts returned in the completion block
 */
- (void)findContactsWithPhoneNumbers:(NSArray *)phoneNumbers emailAddresses:(NSArray *)emailAddresses completion:(DKFindContactsCompletionBlock)completion saveAutomatically:(BOOL)saveAutomatically;

/*!  
 @abstract Finds contacts from the devices address book that are using the Donky Messaging Service
 
 @discussion Uses email addresses and phone numbers from the devices address book to discover contacts that are a member of Donky Messaging Service.
 
 @param completion Called when all found contacts have been saved
 @param saveAutomatically Whether returned contacts are saved automatically to the contact list. If NO then -addFoundContacts: will need to be called seperatly with the contacts returned in the completion block
 */
- (void)findContactsFromAddressBookCompletion:(DKFindContactsCompletionBlock)completion saveAutomatically:(BOOL)saveAutomatically;

/*!  
 @abstract Adds the contacts retrieved from -findContactsCompletion:
 
 @discussion The method should be used with the DKContacts retrieved by findContactsCompletion:
 
 @param contacts An array of DKContacts
 */
- (void)addFoundContacts:(NSArray *)contacts;

/*!  
 @abstract Adds a delegate
 
 @discussion Any added delegate should conform to DKChatControllerDelegate protocol. Delegates are notified using notifyDelegatesWithChangeSet:. Delegates that are nilled off are automatically removed.
 
 @param delegate    An object that conforms to DKContactControllerDelegate
 */
- (void)addDelegate:(id<DKContactControllerDelegate>)delegate;

/*!  
 @abstract Removes a delegate
 
 @param delegate    An object that conforms to DKContactControllerDelegate
 */
- (void)removeDelegate:(id<DKContactControllerDelegate>)delegate;

@end
