//
//  DKUser.h
//  DonkySDK
//
//  Created by David Taylor on 05/12/2013.
//  Copyright (c) 2013 Compsoft. All rights reserved.
//

#import "DKContact.h"

/*! 
 @abstract A class that represents the user
 */
@interface DKUser : DKContact

/*! 
 @abstract An ISO 3166-1 alpha-3 format country code
 */
@property (strong, nonatomic) NSString *countryCode;

@end
