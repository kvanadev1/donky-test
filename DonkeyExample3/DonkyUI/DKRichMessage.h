//
//  DKRichMessage.h
//  DonkySDK
//
//  Created by David Taylor on 04/12/2013.
//  Copyright (c) 2013 Compsoft. All rights reserved.
//

#import <Foundation/Foundation.h>

@class DKAsset, DKContact;

/*! 
 @abstract A class that represents a rich message
 */
@interface DKRichMessage : NSObject

/*! 
 @abstract The main content of the rich message
 */
@property (nonatomic, retain) NSString * body;

/*! 
 @abstract Whether the message can be forwarded to other contacts
 */
@property (nonatomic) BOOL canForward;

/*! 
 @abstract Whether the message can be replied to
 */
@property (nonatomic) BOOL canReply;

/*! 
 @abstract A date/time (UTC) for when the message body has expired and will be replaced with an expired body
 */
@property (nonatomic, retain) NSDate * expiryTimestamp;

/*! 
 @abstract The expired content of the rich message, shown after the expiryTimestamp has passed
 */
@property (nonatomic, retain) NSString * expiredBody;

/*! 
 @abstract The display name
 */
@property (nonatomic, retain) NSString * forwardedByDisplayName;

/*! 
 @abstract The external user id of whoever forwarded the rich message
 */
@property (nonatomic, retain) NSString * forwardedByExternalUserId;

/*! 
 @abstract The message that
 */
@property (nonatomic, retain) NSString * forwardingOverlayMessage;

/*! 
 @abstract Whether the message has been read or not
 */
@property (nonatomic) BOOL hasBeenRead;

/*! 
 @abstract The short text description of the message
 */
@property (nonatomic, retain) NSString * messageDescription;

/*! 
 @abstract The date/time (UTC) the message was sent to the service
 */
@property (nonatomic, retain) NSDate * sentTimestamp;

/*! 
 @abstract the display name of the sender
 */
@property (nonatomic, retain) NSString * senderDisplayName;

/*! 
 @abstract The external user id of the sender
 */
@property (nonatomic, retain) NSString * senderExternalUserId;

/*! 
 @abstract The unique message id
 */
@property (nonatomic, retain) NSString * messageId;

/*! 
 @abstract The message type
 */
@property (nonatomic, retain) NSString * messageType;

/*! 
 @abstract The messages avatar
 */
@property (nonatomic, retain) DKAsset *avatarAsset;

/*! 
 @abstract The contact who forwarded the rich message
 */
@property (nonatomic, retain) DKContact *forwardContact;

/*! 
 @abstract Rich messages attachments
 */
@property (nonatomic, retain) NSSet *assets;

@end