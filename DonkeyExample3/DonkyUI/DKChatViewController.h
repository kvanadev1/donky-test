//
//  DKChatViewController.h
//  DonkySDK
//
//  Created by David Taylor on 02/12/2013.
//  Copyright (c) 2013 Compsoft. All rights reserved.
//

#import "DKBaseViewController.h"
#import "DKDonkyClient.h"
#import "DKLocalNotificationController.h"

@class DKChatViewController;

/*! 
 @abstract Events raised by DKChatViewController
 */
@protocol DKChatViewControllerDelegate <NSObject>

@optional
/*!  
 @abstract Called when the user taps an image
 
 @param chatViewController The DKChatViewController that received the tap
 @param imagePath          The file path of the image tapped
 */
- (void)chatViewController:(DKChatViewController *)chatViewController didSelectImageWithPath:(NSString *)imagePath;

@end

/*! 
 @abstract Acts as controller for capturing new messages and sending them on and showing the chat history
 */
@interface DKChatViewController : DKBaseViewController <UITableViewDelegate, UIImagePickerControllerDelegate, DKChatControllerDelegate, DKLocalNotificationControllerDelegate>

/*! 
 @abstract Overrides events raised by DKChatViewController
 */
@property (weak, nonatomic) id <DKChatViewControllerDelegate> delegate;

/*! 
 @abstract The conversation thread to display
 */
@property (nonatomic, strong) DKConversationThread *conversationThread;

/*! 
 @abstract Sets the contacts that the chat message will be broadcasted to
 */
@property (nonatomic, strong) NSArray *contacts;

/*! 
 @abstract Sets the chat messages to be displayed
 */
@property (nonatomic, strong) NSArray *chatMessages;

/*!  
 @abstract Adds the contacts to DKChatViewControllers recipient lists
 
 @param contacts An array of DKContacts
 */
- (void)addContacts:(NSArray *)contacts;

@end
