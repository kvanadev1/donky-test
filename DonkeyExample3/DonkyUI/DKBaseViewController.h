//
//  DKBaseViewController.h
//  DonkyUI
//
//  Created by David Taylor on 06/11/2013.
//  Copyright (c) 2013 Compsoft. All rights reserved.
//

#import <UIKit/UIKit.h>

@class DKTheme;

@interface DKBaseViewController : UIViewController <UIAlertViewDelegate>

@end
