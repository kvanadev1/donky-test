//
//  DKChatMessage.h
//  DonkySDK
//
//  Created by Tristan Warner-Smith on 19/12/2013.
//  Copyright (c) 2013 Compsoft. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DKConstants.h"

typedef enum {
    DKGroupNotificationNil,
    DKGroupNotificationAdd,
    DKGroupNotificationRemove,
}DKGroupNotification;

@class DKContact;

/*! 
 @abstract A class that represents a chat message
 */
@interface DKChatMessage : NSObject

/*! 
 @abstract Whether the chat is a single or group chat
 */
@property (nonatomic) DKChatType chatType;

/*! 
 @abstract The message body as specified by the sender
 */
@property (nonatomic, retain) NSString * body;

/*! 
 @abstract The conversation identifier
 */
@property (nonatomic, retain) NSString * conversationId;

/*! 
 @abstract The unique message Id
 */
@property (nonatomic, retain) NSString * messageId;

/*! 
 @abstract The message type
 */
@property (nonatomic, retain) NSString * messageType;

/*! 
 @abstract the date/time (UTC) that the message was sent to the service
 */
@property (nonatomic, retain) NSDate * sentTimestamp;

/*! 
 @abstract Whether the message is being sent / sent / read / failed to send etc
 */
@property (nonatomic) DKMessageState state;

/*! 
 @abstract Any attachments that the message has
 */
@property (nonatomic, retain) NSArray *assets;

/*! 
 @abstract Whether the message was sent by the user
 */
@property (nonatomic) BOOL isUserMessage;

/*! 
 @abstract Whether the user can reply to the message
 */
@property (nonatomic) BOOL canReply;

/*! 
 @abstract The sender of the message
 */
@property (nonatomic) DKContact *contact;

/*! 
 @abstract If DKGroupNotificationAdd or DKGroupNotificationRemove then the message is a notification informing the user someone has either joined or left the group chat. Not applicable to single chats
 */
@property (nonatomic) DKGroupNotification groupNotification;

/*! 
 @abstract The time that the message expires, from then on the body will be set to a different expired body
 */
@property (nonatomic, retain) NSDate *expiryTimestamp;

/*!  
 @abstract Whether the message has been read or not
 */
- (BOOL)hasBeenRead;

@end