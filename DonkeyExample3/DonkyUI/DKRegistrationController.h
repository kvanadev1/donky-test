//
//  DKRegistrationController.h
//  DonkySDK
//
//  Created by David Taylor on 13/11/2013.
//  Copyright (c) 2013 Compsoft. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "DKConstants.h"

@class DKUser;

/*! 
 @abstract Responsible for user registration and login
 */
@interface DKRegistrationController : NSObject

/*!  
 @abstract Checks whether the user is registered with Donky Messaging Service
 
 @return YES if the user is registered with Donky Messaging Service
 */
- (BOOL)isRegistered;

/*!  
 @abstract Registers the user with Donky Messaging Service
 
 @discussion Pass in nil for user to register anonymously
 
 @param user       A filled out DKUser object
 @param completion Called when registration has completed or failed
 */
- (void)registerWithUser:(DKUser *)user completion:(DKCompletionBlock)completion;

/*!  
 @abstract Logs the user in provided they have already registered
 
 @param completion Called when login has completed or failed
 */
- (void)login:(DKCompletionBlock)completion;

/*!  
 @abstract Checks whether the user is logged into Donky Messaging N
 
 @return YES if the user is logged into Donky Messaging Service
 */
- (BOOL)isLoggedIn;

/*!  
 @return Whether the user is logging in
 */
- (BOOL)isLoggingIn;

/*!  
 @return Whether the user is in the process of registering
 */
- (BOOL)isRegistering;

/*!  
 @abstract Unregisters the user from Donky Messaging Service
 
 @param completion Caled when unregister has completed
 */
- (void)unregister:(DKCompletionBlock)completion;

/*!  
 @abstract Cancel the current login or register request (if a request is currently running).
 */
- (void)cancelLoginOrRegister;

@end
