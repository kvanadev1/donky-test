//
//  DKLocalNotificationController.h
//  DonkySDK
//
//  Created by David Taylor on 30/01/2014.
//  Copyright (c) 2014 Compsoft. All rights reserved.
//

#import <Foundation/Foundation.h>

@class DKConversationThread;

/*!
 Keys used To populate the userInfo in a UILocalNotification
 */
extern NSString * const kDKLocalNotificationConversationId;
extern NSString * const kDKLocalNotificationRichMessageId;
extern NSString * const kDKLocalNotificationDisplayName;
extern NSString * const kDKLocalNotificationMessage;
extern NSString * const kDKLocalNotificationAvatarAssetId;
extern NSString * const kDKLocalNotificationSoundPath;
extern NSString * const kDKLocalNotificationVibration;

/*! 
 @abstract Provides a protocol for working out which notifications to display
 */
@protocol DKLocalNotificationControllerDelegate <NSObject>

@optional
/*!  
 @return Return the conversation thread that is currently being viewed
 */
- (DKConversationThread *)conversationThreadViewed;

/*!  
 @return Return YES to display rich message notifications
 */
- (BOOL)shouldDisplayRichMessageNotifications;

/*!  
 @return Return YES to display chat message notifications
 */
- (BOOL)shouldDisplayChatMessageNotifications;

@end

/*! 
 @abstract Handles the creation and scheduling of local notifications when new messages are downloaded
 */
@interface DKLocalNotificationController : NSObject

/*! 
 @abstract A view controller that presents the UIViewController that is currently being displayed. This is used by the SDK to work out which message notifications to display on the UIViewController
 */
@property (weak, nonatomic) id <DKLocalNotificationControllerDelegate> localNotificationsDelegateViewController;

@end
