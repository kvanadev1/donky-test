//
//  DKDonkyClient.h
//  DonkySDK
//
//  Created by David Taylor on 11/11/2013.
//  Copyright (c) 2013 Compsoft. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "DKConstants.h"
#import "DKRegistrationController.h"
#import "DKRichMessageController.h"
#import "DKAssetController.h"
#import "DKUserController.h"
#import "DKContactController.h"
#import "DKAPNSController.h"
#import "DKChatController.h"
#import "DKLocalNotificationController.h"
#import "DKConfigurationController.h"

#import "DKMessageCounts.h"
#import "DKNotificationSettings.h"
#import "DKAppUserDetails.h"

#import "DKContact.h"
#import "DKChatMessage.h"
#import "DKRichMessage.h"
#import "DKAsset.h"
#import "DKUser.h"

/*! 
 @abstract Main access point to the API.
 */
@interface DKDonkyClient : NSObject

/*!  
 @abstract Setups up the SDK with the given API key and config
 
 @discussion This must be called before using any other methods in Donky SDK
 
 @param apiKey        Your apps unqiue key assigned to you by Donky Messaging Service
 @param configuration A dictionary of configuration options
 @param completion    Callback when the SDK has initialized
 */
+ (void)initializeWithAPIKey:(NSString *)apiKey configuration:(NSDictionary *)configuration completion:(DKCompletionBlock)completion;

/*!  
 @abstract Used to access all Donky SDK functions
 
 @return An instance of DKDonkyClient
 */
+ (DKDonkyClient *)sharedInstance;

/*!  
 @abstract Checks whether the specified external user id is registered in the Application space and if it is retrieves the details and adds it to the clients known contacts.
           You must have registered as a delegate with DKContactController to get the result.
 
 @param externalUserId The contacts external user id.
 @param completion (Optional) Called on completion. Error codes: DKContactControllerErrorCodeContactIsUser, DKContactControllerErrorCodeContactAlreadyAdded, DKContactControllerErrorCodeInvalidUser
 */
- (void)addContact:(NSString *)externaluserId completion:(DKAddContactCompletionBlock)completion;

/*!  
 @abstract Adds the specified participants to the specified group chat.
 
 @param contacts The contacts external user id.
 @param conversationId The group conversation identifier
 @param completion (Optional) Called on completion. Error codes: DKChatControllerErrorCodeInvalidConversationId, DKChatControllerErrorCodeNoValidContactsSpecified
 */
- (void)addContacts:(NSArray *)contacts toGroupWithConversationId:(NSString *)conversationId completion:(DKCompletionBlock)completion;

/*!  
 @abstract Creates a new group chat with the specified participants.
 
 @param contacts The recipients of the group chat
 @param error (Optional) On an error retrieving the contacts or adding them to the conversation. Codes: DKChatControllerErrorCodeNoValidContactsSpecified
 */
- (DKConversationThread *)createGroupChatWithContacts:(NSArray *)contacts error:(NSError **)error;

/*!  
 @abstract Creates a new single chat with the specified participant.
 
 @param contact The recipient of the chat.
 @param error (Optional) On an error retrieving the contacts or adding them to the conversation. Codes: DKChatControllerErrorCodeNoValidContactsSpecified
 */
- (DKConversationThread *)createSingleChatWithContact:(NSString *)contact error:(NSError **)error;

/*!  
 @abstract Deletes the specified rich message.
 
 @param messageId The message id to be deleted.
 @param error (Optional) On an error deleting the DKRichMessage. Codes: DKRichMessageControllerErrorCodeInvalidMessageId
 */
- (void)deleteRichMessage:(NSString *)messageId error:(NSError **)error;

/*!  
 @abstract Deletes the specified chat or group chat conversation.
 
 @param conversationId The conversation id to be deleted.
 @param error (Optional) On an error deleting the DKRichMessage. Codes: DKChatControllerErrorCodeInvalidConversationId
 */
- (void)deleteConversation:(NSString *)conversationId error:(NSError **)error;

/*!  
 @abstract Retrieves an enumerator for chat messages that match the partial filter text specified anywhere within the last messages of the conversations body.
 
 @param filter (Optional) The partial match filter text to filter the results to.
 @param offset The offset to start picking the conversations.
 @param limit The maximum number of conversation to return.
 @param error (Optional) On an error retrieving the data.
 
 @return An array of DKConversationThread
 */
- (NSArray *)filterChatConversations:(NSString *)filter offset:(NSUInteger)offset limit:(NSUInteger)limit error:(NSError **)error;

/*!  
 @abstract Retrieves an enumerator into the rich messages that match the partial filter text specified anywhere within the message body.
 
 @param filter (Optional) The partial match filter text to filter the results to.
 @param offset The offset to start picking the conversations.
 @param limit The maximum number of conversation to return
 @param error (Optional) On an error retrieving the data.
 
 @return An array of DKRichMessage
 */
- (NSArray *)filterRichMessages:(NSString *)filter offset:(NSUInteger)offset limit:(NSUInteger)limit error:(NSError **)error;

/*!  
 @abstract Retrieves an enumerator into the contacts that match the partial filter text specified anywhere within the contacts name.
 
 @param filter (Optional) The partial match filter text to filter the results to.
 @param offset The offset to start picking the conversations.
 @param limit The maximum number of conversation to return
 @param error (Optional) On an error retrieving the data.
 
 @return An array of DKContact
 */
- (NSArray *)filterContacts:(NSString *)filter offset:(NSUInteger)offset limit:(NSUInteger)limit error:(NSError **)error;

/*!  
 @abstract Searches the DMN Application space for matches for the supplied email addresses and / or phone numbers.
    This is a server call to allow the phone numbers and email addresses to be used to find matching App Users in the Donky Networks Application Space.
 
 @param phoneNumbers (Optional) The phone numbers to try and find a match for.
 @param emailAddresses (Optional) The email addresses to try and find a match for.
 @param completion (Optional) Called on completion
 */
- (void)findContactsWithPhoneNumbers:(NSArray *)phoneNumbers emailAddresses:(NSArray *)emailAddresses completion:(DKFindContactsCompletionBlock)completion;

/*!  
 @abstract Replys to a rich message
 
 @param richMessageId The id of the rich message to reply to
 @param messageText The reply message
 @param attachments (Optional) An array of reply attachments
 @param completion (Optional) Called when the message has been sent or on error. Error codes: DKRichMessageControllerErrorCodeInvalidMessageId, DKRichMessageControllerErrorCodeMessageExpired, DKRichMessageControllerErrorCodeNoMessageSpecified
 
 @return The new conversation thread
 */
- (DKConversationThread *)replyToRichMessage:(NSString *)richMessageId messageText:(NSString *)messageText assetIds:(NSArray *)assetIds completion:(DKCompletionBlock)completion;

/*!  
 @abstract Forwards the specified message to the specified recipients.
 
 @param messageId The message id to the message to be forwarded.
 @param recipients The contacts to forward the message to.
 @param comment (Optional) Additional forwarding comment to add to the message with a max length of 160 chars.
 @param completion (Optional) Called on completion. Error codes: DKRichMessageControllerErrorCodeInvalidMessageId, DKRichMessageControllerErrorCodeMessageExpired, DKRichMessageControllerErrorCodeNoMessageSpecified, DKRichMessageControllerErrorCodeNoValidContactsSpecified
 */
- (void)forwardRichMessage:(NSString *)messageId toRecipients:(NSArray *)recipients comment:(NSString *)comment completion:(DKCompletionBlock)completion;

/*!  
 @abstract Returns the details for the current user if registered.
 
 @return The details for the current user if registered
 */
- (DKAppUserDetails *)appUserDetails;

/*!  
 @abstract Loads the data for the specified attachment.
 
 @param attachmentId The attachment id to be retrieved.
 @param completion (Optional) Called on completion. Error codes: DKAssetControllerErrorInvalidAssetId
 */
- (void)getAttachment:(NSString *)attachmentId completion:(DKAssetDownloadCompletion)completion;

/*!  
 @abstract Retrieves available chat/group chat message threads (conversation) details.
 
 @param offset The offset to start picking the conversations.
 @param limit The maximum number of conversation to return
 @param error (Optional) On an error retrieving the conversations
 
 @return An array of DKConversationThread
 */
- (NSArray *)allConversationsWithOffset:(NSUInteger)offset limit:(NSUInteger)limit error:(NSError **)error;

/*!  
 @abstract Retrieves available rich message details.
 
 @param offset The offset to start picking the rich messages.
 @param limit The maximum number of rich messages to return
 @param error (Optional) On an error retrieving the rich messages
 
 @return An array of DKRichMessage
 */
- (NSArray *)allRichMessagesWithOffset:(NSUInteger)offset limit:(NSUInteger)limit error:(NSError **)error;

/*!  
 @abstract Retrieves available chat messages for a conversation thread.
 
 @param conversationId The conversation id the messages are required for
 @param offset The offset to start picking the rich messages.
 @param limit The maximum number of rich messages to return
 @param error (Optional) On an error retrieving the rich messages
 
 @return An array of DKChatMessage
 */
- (NSArray *)chatMessagesForConversation:(NSString *)conversationId withOffset:(NSUInteger)offset limit:(NSUInteger)limit error:(NSError **)error;

/*!  
 @abstract Retrieves available group chat messages for a conversation thread.
 
 @param conversationId The conversation id the messages are required for
 @param offset The offset to start picking the rich messages.
 @param limit The maximum number of rich messages to return
 @param error (Optional) On an error retrieving the rich messages
 
 @return An array of DKChatMessage
 */
- (NSArray *)groupMessagesForConversation:(NSString *)conversationId withOffset:(NSUInteger)offset limit:(NSUInteger)limit error:(NSError **)error;

/*!  
 @abstract Retrieves the counts of messages and the amount of unread messages.
 
 @return The counts of messages and the amount of unread messages.
 
 @see RichMessageController#getMessageCount()
 @see RichMessageController#getUnreadMessageCount()
 @see MessageController#getConversationCount()
 @see MessageController#getUnreadConversationCount()
 */
- (DKMessageCounts *)messageCounts;

/*!  
 @abstract Retrieves the App Users notification settings for new messages.
 
 @return The App Users notification settings for new messages.
 */
- (DKNotificationSettings *)notificationSettings;

/*!  
 @abstract Retrieves the list of tags for the Application space, and indicates which the App User has indicated interest in.
 
 @param completion Called on completion
 */
- (void)getTags:(DKTagsCompletion)completion;

/*!  
 @return Whether the user is logging in
 */
- (BOOL)isLoggingIn;

/*!  
 @return Whether the user is in the process of registering
 */
- (BOOL)isRegistering;

/*!  
 @abstract Checks the logged in state of the Donky client and returns true if the client has a valid login and token currently, otherwise it returns false.
 
 @return YES, if the user is logged in, NO otherwise.
 */
- (BOOL)isLoggedIn;

/*!  
 @abstract Checks the registration state of the Donky client and returns true if the client has a valid registration state currently, otherwise it returns false.
 
 @return True, if the user is registered, false otherwise.
 */
- (BOOL)isRegistered;

/*!  
 @abstract Removes the registered user from the specified group chat.
 
 @param conversationId The conversation id for the group chat.
 @param completion Called on completion
 */
- (void)leaveGroup:(NSString *)conversationId completion:(DKCompletionBlock)completion;

/*!  
 @abstract This method will login an App User using the registration state information of the client.
    If the login cannot occur due to insufficient state information, then a false value is returned.
 
 @param externalUserId The contacts external user id.
 @param completion Called on completion
 */
- (void)login:(DKCompletionBlock)completion;

/*!  
 @abstract This method will register and login an App User using the details supplied and save the registration state to the client.
 
 @param externalUserID (Optional if displayname is specified) The external user id they wish to use for registration (optional)
 @param displayName (Optional) The display name to use for the App User (Optional). If not passed it should be set to the same as the External User Id
 @param countryCode (Optional) ISO country code for the App User (Optional); this should be derived from the device if possible if not passed.
 @param emailAddress (Optional) The email address of the App User (Optional)
 @param phoneNumber (Optional) The mobile phone number of the App User (Optional)
 @param additionalProperties (Optional) A dictionary of ad-hoc properties to be associated with the user
 @param completion (Optional) Called on completion
 */
- (void)registerWithExternalUserId:(NSString *)externalUserId displayName:(NSString *)displayName countryCode:(NSString *)countryCode emailAddress:(NSString *)emailAddress phoneNumber:(NSString *)phoneNumber additionalProperties:(NSDictionary *)additionalProperties completion:(DKCompletionBlock)completion;

/*!  
 @abstract This method allows the integrator to send a chat message.
 
 @param conversationId The conversation to send the message to.
 @param message Message text to send.
 @param attachments (Optional) A collection of images to send with the message as attachments.
 @param completion (Optional) Called on completion. Error Codes: DKChatControllerErrorCodeNoValidContactsSpecified, DKChatControllerErrorCodeNoMessageSpecified, DKChatControllerErrorCodeMaxAttachmentsExceeded
 */
- (void)sendMessage:(NSString *)message withAttachments:(NSArray *)attachments toConversation:(NSString *)conversationId completion:(DKCompletionBlock)completion;

/*!  
 @abstract Saves a message and a list of assets against a conversation thread that can be retrieved later
 
 @param message The message to save, gets set to nil ager sending the message
 @param assetIds (Optional) The Ids of the assets to save, gets set to nil ager sending the message
 @param conversationThreadId The Id of the conversation thread to save against
 @param error (Optional) On error saving the message. Codes: DKChatControllerErrorCodeInvalidConversationId
 */
- (void)saveMessage:(NSString *)message assets:(NSArray *)assetIds forConversationThreadWithId:(NSString *)conversationThreadId error:(NSError **)error;

/*!  
 @abstract Sets the display name for the current user if registered, similar to the settings screen.
 
 @param avatarFile The File name and path to the new avatar
 @param completion (Optional) Called on completion. Error codes: DKUserControllerErrorCodeDisplayNameMissing
 */
- (void)setDisplayName:(NSString *)displayName completion:(DKCompletionBlock)completion;

/*!  
 @abstract This method sets a new image for the App Users avatar; it will resize the submitted binary image to match the required format and size.
 
 @param externalUserId The contacts external user id.
 @param completion (Optional) Called on completion. Error codes: DKUserControllerErrorCodeFileNotFound
 */
- (void)setAvatar:(NSString *)filePath completion:(DKCompletionBlock)completion;

/*!  
 @abstract Remove the users avatar asset ID. This updates the service in the background.
 
 @param completion (Optional) Called on completion
 */
- (void)removeAvatarOnCompletion:(DKCompletionBlock)completion;

/*!  
 @abstract Sets the App Users notification settings for new messages.
 
 @param soundPath The filepath of the sound that will be played when a new message arrives.
 @param vibration Indicates whether the device will vibrate.
 @param completion (Optional) Called on completion
 */
- (void)setNotificationSettingsWithSoundPath:(NSString *)soundPath vibration:(BOOL)vibration completion:(DKCompletionBlock)completion;

/*!  
 @return Has the user allowed push notifications (defaults to YES).
 */
- (BOOL)isPushEnabled;

/*!  
 @abstract Turn push notifications on or off. This updates the service in the background
 
 @param state Turn push notifications on or off.
 @param completion (Optional) Called on completion of the service call
 */
- (void)setPushState:(BOOL)state completion:(DKCompletionBlock)completion;

/*!  
 @abstract Load an image asset. This will retrieve the image from cache or the service in the background and call the delegate back on the UI thread.
 
 @param assetId The ID of the asset to load
 @param completion (Optional) Called when the asset has been loaded. Error codes: DKAssetControllerErrorInvalidAssetId
 */
- (void)getAssetData:(NSString *)assetId completion:(DKAssetDownloadCompletion)completion;

/*!
 @abstract Uploads an asset to the service
 
 @param filePath   File path for the image to be uploaded
 @param completion A callback that holds the uploaded asset information and any errors. Error codes: DKAssetControllerErrorCodeFileNotFound
 */
- (void)uploadAssetPath:(NSString *)filePath completion:(DKAssetUploadCompletion)completion;

/*!  
 @abstract Get a contact by ID.
 
 @param id    The contact external Id
 @param error (Optional) On error getting the contact
 
 @return The contact, or nil if the ID doesn't exist.
 */
- (DKContact *)contact:(NSString *)id error:(NSError **)error;

/*!  
 @abstract Get an array of contacts by ID.
 
 @param ids   The contacts external Ids
 @param error (Optional) On error getting the contact
 
 @return An array of DKContact.
 */
- (NSArray *)contacts:(NSArray *)ids error:(NSError **)error;

/*!  
 @abstract Delete a contact
 
 @param id    the contact to delete
 @param error (Optional) On an error deleting the contact. Codes: DKContactControllerErrorCodeInvalidUser
 */
- (void)deleteContact:(NSString *)id error:(NSError **)error;

/*!  
 @abstract Mark a conversation thread as being read by the user and save it's state.
 
 @param conversation The conversation thread to mark as read
 @param completion (Optional) Called on completion after informing the service the conversation thread has been read
 */
- (void)markAsRead:(DKConversationThread *)conversation completion:(DKCompletionBlock)completion;

/*!  
 @abstract Get all contacts that are participating in the given conversation thread.
 
 @param conversationThread The conversation thread to get user IDs for.
 
 @return An aray of DKContact
 */
- (NSArray *)participantsForConversation:(DKConversationThread *)conversationThread;

/*!  
 @abstract Cancel the current login or register request (if a request is currently running).
 */
- (void)cancelLoginOrRegister;

/*!  
 @abstract Registers the device with the Donky Messaging Network
 
 @param token The device token received after registering for remote notifications
 */
- (void)registerAPNS:(NSData *)token;

/*!  
 @abstract Filters out and acts upon Donky related remote notifications
 
 @param userInfo The remote notification to handle
 */
- (void)handleNotification:(NSDictionary *)userInfo;

/*!  
 @abstract set delegate for apns methods
 
 @param delegate The delegate to set
 */
- (void)setAPNSDelegate:(id<DKAPNSControllerDelegate>)delegate;

/*!  
 @abstract Get a rich message by id
 
 @param id The message ID
 
 @return The rich message, or nil if the ID doesn't exist
 */
- (DKRichMessage *)richMessage:(NSString *)id;

/*!  
 @abstract markRichMessageAsRead:completion:
 
 @param richMessage The rich message to mark as read
 @param completion  (Optional) Called on completion after informing the service the rich message has been read
 */
- (void)markRichMessageAsRead:(DKRichMessage *)richMessage completion:(DKCompletionBlock)completion;

/*!  
 @abstract Sets the users tag preferences to the list specified.
 
 @param @param tags The tag list and the App users preferences.
 @param completion (Optional) Called on completion
 */
- (void)setTags:(NSArray *)tags completion:(DKCompletionBlock)completion;

/*!  
 @abstract Add the delegate for contact events.
 
 @param delegate The delegate to add.
 */
- (void)addContactDelegate:(id<DKContactControllerDelegate>)delegate;

/*!  
 @abstract Remove the delegate for contact events.
 
 @param delegate The delegate to remove.
 */
- (void)removeContactDelegate:(id<DKContactControllerDelegate>)delegate;

/*!  
 @abstract Add the delegate for chat events.
 
 @param delegate The delegate to add.
 */
- (void)addChatDelegate:(id<DKChatControllerDelegate>)delegate;

/*!  
 @abstract Remove the delegate for chat events.
 
 @param delegate The delegate to remove.
 */
- (void)removeChatDelegate:(id<DKChatControllerDelegate>)delegate;

/*!  
 @abstract Add the delegate for rich message events.

 @param delegate The delegate to add.
 */
- (void)addRichMessageDelegate:(id<DKRichMessageControllerDelegate>)delegate;

/*!  
 @abstract Remove the delegate for rich message events.
 
 @param delegate The delegate to remove.
 */
- (void)removeRichMessageDelegate:(id<DKRichMessageControllerDelegate>)delegate;

/*! 
 @abstract Sets a delegate for local notification customisation
 
 @param The UIViewController that is currently being displayed. This is used by the SDK to work out which message notifications to display to the user.
 */
- (void)setLocalNotificationsDelegateViewController:(id<DKLocalNotificationControllerDelegate>)localNotificationsDelegateViewController;

/*!  
 @abstract Controller for APNS related functions
 */
- (DKAPNSController *)apnsController;

/*!  
 @abstract Controller for asset uploading and download functions
 */
- (DKAssetController *)assetController;

/*!  
 @abstract Controller for chat related functions
 */
- (DKChatController *)chatController;

/*!  
 @abstract Controller for contact management related functions
 */
- (DKContactController *)contactController;

/*!  
 @abstract Controller for registration functions
 */
- (DKRegistrationController *)registrationController;

/*!  
 @abstract Controller for rich message related functions
 */
- (DKRichMessageController *)richMessageController;

/*!  
 @abstract Controller for user related options
 */
- (DKUserController *)userController;

/*!  
 @abstract Controller for local notification related functions
 */
- (DKLocalNotificationController *)localNotificationController;

/*!  
 @abstract Controller that provides access to configuration options for use by the UI portion of the SDK
 */
- (DKConfigurationController *)configurationController;

/*!  
 @abstract A mechanism for sending and recieving Donky related notificaitons
 
 @param completion Called when the notification exchange has completed
 */
- (void)runNotificationExchangeWithCompletion:(DKCompletionBlock)completion;

#pragma mark - Error codes

/*!
 Error codes and domains returned by methods in DKDonkyClient
 */
typedef enum {
    DKRegistrationControllerErrorCodeNotRegistered,
    DKRegistrationControllerErrorCodeNotLoggedIn,
    DKRegistrationControllerErrorCodeDeviceRegistering,
    DKRegistrationControllerErrorCodeDeviceLoggingIn,
    DKRegistrationControllerErrorCodeUserSuspended
} DKRegistrationControllerErrorCodes;

extern NSString * const DKRegistrationControllerErrorDomain;


typedef enum {
    DKRichMessageControllerErrorCodeInvalidMessageId,
    DKRichMessageControllerErrorCodeMessageExpired,
    DKRichMessageControllerErrorCodeNoValidContactsSpecified,
    DKRichMessageControllerErrorCodeNoMessageSpecified,
} DKRichMessageControllerErrorCodes;

extern NSString * const DKRichMessageControllerErrorDomain;


typedef enum {
    DKChatControllerErrorCodeInvalidConversationId,
    DKChatControllerErrorCodeNoMessageSpecified,
    DKChatControllerErrorCodeMaxAttachmentsExceeded,
    DKChatControllerErrorCodeNoValidContactsSpecified,
} DKChatControllerErrorCodes;

extern NSString * const DKChatControllerErrorDomain;


typedef enum {
    DKContactControllerErrorCodeContactAlreadyAdded,
    DKContactControllerErrorCodeContactIsUser,
    DKContactControllerErrorCodeInvalidUser,
} DKContactControllerErrorCodes;

extern NSString * const DKContactControllerErrorDomain;


typedef enum {
    DKUserControllerErrorCodeFileNotFound,
    DKUserControllerErrorCodeImageMissing,
    DKUserControllerErrorCodeDisplayNameMissing,
} DKUserControllerErrorCodes;

extern NSString * const DKUserControllerErrorDomain;


typedef enum {
    DKAssetControllerErrorCodeFileNotFound,
    DKAssetControllerErrorInvalidAssetId,
} DKAssetControllerErrorCodes;

extern NSString * const DKAssetControllerErrorDomain;

@end
