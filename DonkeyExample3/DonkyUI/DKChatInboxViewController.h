//
//  DKChatInboxViewController.h
//  DonkySDK
//
//  Created by David Taylor on 10/12/2013.
//  Copyright (c) 2013 Compsoft. All rights reserved.
//

#import "DKBaseTableViewController.h"
#import "DKDonkyClient.h"

@class DKChatInboxViewController;

/*! 
 @abstract Events raised by DKChatInboxViewController
 */
@protocol DKChatInboxViewControllerDelegate <NSObject>

@optional
/*!  
 @abstract Called when the user attempts to create a new conversation thread
 
 @param chatInboxViewController The DKChatInboxViewController that received the tap
 */
- (void)didSelectComposeConversationThreadInChatInboxViewController:(DKChatInboxViewController *)chatInboxViewController;

/*! 
 @abstract Called when the user taps on a conversation thread

 @param chatInboxViewController The DKChatInboxViewController that received the tap
 @param conversationThread      The conversation thread tapped
*/
- (void)chatInboxViewController:(DKChatInboxViewController *)chatInboxViewController didSelectConversationThread:(DKConversationThread *)conversationThread;

@end

/*! 
 @abstract Displays a list of conversation threads
 */
@interface DKChatInboxViewController : DKBaseTableViewController <UISearchDisplayDelegate, UIScrollViewDelegate, DKChatControllerDelegate, UIAlertViewDelegate, DKLocalNotificationControllerDelegate>

/*! 
 @abstract Overrides events raised by DKChatInboxViewController
 */
@property (weak, nonatomic) id <DKChatInboxViewControllerDelegate> delegate;

/*!  
 @abstract Hightlights the specified conversation thread in the list
 
 @param conversationThread The conversation thread to highlight
 */
- (void)highlightConversationThread:(DKConversationThread *)conversationThread;

@end
