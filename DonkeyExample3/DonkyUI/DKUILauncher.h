//
//  DKUILauncher.h
//  DonkyUI
//
//  Created by David Taylor on 21/11/2013.
//  Copyright (c) 2013 Compsoft. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum {
    DKDonkyClientViewControllerOptionRichMessageList,
    DKDonkyClientViewControllerOptionChatInbox,
    DKDonkyClientViewControllerOptionContactList,
    DKDonkyClientViewControllerOptionSettings,
}DKDonkyClientViewControllerOption;

@class DKDonkyClientViewController, DKRichMessageListViewController, DKViewRichMessageViewController, DKRichMessageViewController_iPad, DKChatInboxViewController, DKChatViewController, DKChatViewController_iPad, DKRichMessage, DKConversationThread, DKSettingsViewController, DKSettingsViewController_iPad, DKContactListViewController, DKNavigationController;

/*! 
 @abstract Creates the different UI components
 */
@interface DKUILauncher : NSObject

#pragma mark - UIViewController creation methods

/*!  
 @param Returns a UIViewController or UITabBarController for the passed option. If a UITabBarController it will show he default tab that is displayed, if the UITabBarController cannot be displayed due to config options then the first is selected
 
 @return A UIViewController that houses the full Donky UI experience
 */
- (UIViewController *)donkyClientViewControllerForOption:(DKDonkyClientViewControllerOption)option;

/*!  
 @return A UITableViewController that displays a list of rich messages
 */
- (DKRichMessageListViewController *)richMessageListViewController;

/*!  
 @param richMessage The rich message that will be loaded
 
 @return A UIViewController that displays a single rich message
 */
- (DKViewRichMessageViewController *)viewRichMessageViewController:(DKRichMessage *)richMessage;

/*!  
 @return A UIViewController that contains both a DKRichMessageListViewController and a DKViewRichMessageViewController
 */
- (DKRichMessageViewController_iPad *)richMessageViewController_iPad;

/*!  
 @return A UITableViewController that displays a list of chat messages
 */
- (DKChatInboxViewController *)chatInboxViewController;

/*!  
 @return A UIViewController that can display a single conversation thread
 */
- (DKChatViewController *)chatViewController;

/*!  
 @return A UIViewController that contains both a DKChatInboxViewController and a DKChatViewController
 */
- (DKChatViewController_iPad *)chatViewController_iPad;

/*!  
 @return A UIViewController that allows the user to set their display name, avatar, notification configurations and tags
 */
- (DKSettingsViewController *)settingsViewController;

/*!  
 @return A UIViewController tailored for an iPad that allows the user to set their display name, avatar, notification configurations and tags
 */
- (DKSettingsViewController_iPad *)settingsViewController_iPad;

/*!  
 @return A UITableViewController that displays a list of contacts
 */
- (DKContactListViewController *)contactListViewController;

/*!  
 @param rootViewController The UIView controller to display
 
 @return A UINavigationController that has the Donky style
 */
- (DKNavigationController *)navigationControllerWithRootViewController:(UIViewController *)rootViewController;

#pragma mark - Navigation to methods

/*!  
 @abstract Displays the rich message list screen
 
 @discussion DKDonkyClientViewController must be the apps key window rootViewController
 
 @param completion      Called when the animation has finished
 
 @return The instance of DKRichMessageListViewController in DKDonkyClientViewController
 */
- (DKRichMessageListViewController *)navigateToRichMessageListViewControllerCompletion:(void (^)(void))completion;

/*!  
 @abstract Displays the chat inbox screen
 
 @discussion DKDonkyClientViewController must be the apps key window rootViewController
 
 @param completion      Called when the animation has finished
 
 @return The instance of DKChatInboxViewController in DKDonkyClientViewController
 */
- (DKChatInboxViewController *)navigateToChatInboxViewControllerCompletion:(void (^)(void))completion;

/*!  
 @abstract Displays the chat screen with the specified conversation thread
 
 @discussion DKDonkyClientViewController must be the apps key window rootViewController
 
 @param conversationId  The Id of the conversation thread to display
 @param completion      Called when the animation has finished
 
 @return The instance of DKChatViewController that is created
 */
- (DKChatViewController *)navigateToChatViewControllerWithConversationId:(NSString *)conversationId completion:(void (^)(void))completion;

/*!  
 @abstract Displays the rich message screen with the specified rich message
 
 @discussion DKDonkyClientViewController must be the apps key window rootViewController
 
 @param richMessageId   The Id of the rich message to display
 @param completion      Called when the animation has finished
 
 @return The instance of DKViewRichMessageViewController that is created
 */
- (DKViewRichMessageViewController *)navigateToRichMessageViewControllerWithRichMessageId:(NSString *)richMessageId completion:(void (^)(void))completion;

@end
