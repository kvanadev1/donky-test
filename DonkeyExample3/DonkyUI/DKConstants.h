//
//  Constants.h
//  DonkySDK
//
//  Created by David Taylor on 15/11/2013.
//  Copyright (c) 2013 Compsoft. All rights reserved.
//

#import <UIKit/UIKit.h>

#ifndef DonkySDK_Constants_h
#define DonkySDK_Constants_h

/*!
 A generic completion block used throughout the SDK
 
 @param error An error raised by the method using the block
 */
typedef void (^DKCompletionBlock) (NSError *error);

/*!
 Message states for the delivery status of chat messages
 */
typedef enum {
    DKNotSent,
    DKFailedToSend,
    DKSent,
    DKRejected,
    DKFailed,
    DKFailedOnHold,
    DKDelivered,
    DKNotRead,
    DKRead
} DKMessageState;

/*!
 The type of chat a conversation thread is
 */
typedef enum {
    DKSingleChat,
    DKGroupChat
} DKChatType;

#endif
