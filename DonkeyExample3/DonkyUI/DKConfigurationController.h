//
//  DKConfigurationController.h
//  DonkySDK
//
//  Created by David Taylor on 20/02/2014.
//  Copyright (c) 2014 Compsoft. All rights reserved.
//

#import <UIKit/UIKit.h>

/*! 
 @abstract Provides access to configuration options for use by the UI portion of the SDK
 */
@interface DKConfigurationController : NSObject

/*!  
 @abstract Indicates whether the App Chat functionality is required of not.
 */
- (BOOL)appChatEnabled;

/*!  
 @abstract Sets the title for the Chat Inbox screen.
 */
- (NSString *)appChatTitle;

/*!  
 @abstract Indicates whether the App Contacts functionality is required of not.
 */
- (BOOL)appContactsEnabled;

/*!  
 @abstract Sets the title for the App Contacts screen.
 */
- (NSString *)appContactsTitle;

/*!  
 @abstract Sets the title for the App Messages screen.
 */
- (NSString *)appMessagesTitle;

/*!  
 @abstract Indicate whether the delete contact option should be displayed on the App Contacts screen.
 */
- (BOOL)deleteContactEnabled;

/*!  
 @abstract Indicate whether the Find Contacts button should be displayed on the App Contacts screen.
 */
- (BOOL)findContactsEnabled;

/*!  
 @abstract This indicates what type of identifier will be used for addressing registered App Users.
 */
- (NSString *)identifierType;

/*!  
 @abstract Indicates whether the invite functionality is required of not.
 */
- (BOOL)inviteEnabled;

/*!  
 @abstract Defines the message text to use when sending an SMS to invite another contact to sign up to the mobile application.
 */
- (NSString *)invitationMessage;

/*!  
 @abstract Indicates whether location information should be acquired and submitted.
 */
- (BOOL)locationTrackingEnabled;

/*!  
 @abstract Indicates whether the manual app user invite functionality is required of not.
 */
- (BOOL)manualContactInviteEnabled;

/*!  
 @abstract User message shown when they have been sending excessive message volumes.
 */
- (NSString *)messageRateExceededMessage;

/*!  
 @abstract Indicates whether the navigation options on the Action bar functionality is required of not.
 */
- (BOOL)navigationEnabled;

/*!  
 @abstract User message shown when the App User turns off native push notifications.
 */
- (NSString *)noPushMessage;

/*!  
 @abstract User message shown when their account is detected as being suspended.
 */
- (NSString *)suspendedUserMessage;

/*!  
 @abstract Used to set how tags will be referred to within the app. i.e. For a premier football app this could be “Teams”
 */
- (NSString *)tagTitle;

/*!  
 @abstract The maximum horizontal resolution that will be accepted for an image attachment.
 */
- (CGFloat)uploadImageMaximumResolutionX;

/*!  
 @abstract The maximum vertical resolution that will be accepted for an image attachment.
 */
- (CGFloat)uploadImageMaximumResolutionY;

@end
