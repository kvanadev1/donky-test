//
//  DKRegistrationViewController.h
//  Donky Client
//
//  Created by Marcin Swierczek on 26/01/2014.
//  Copyright (c) 2014 Dynmark International. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DKKeyboardHandler.h"

@class DKRegistrationViewController;

@protocol DonkyRegistrationDelegate <NSObject>

- (void)registrationViewControllerDidRegister:(DKRegistrationViewController *)registrationViewController;
- (void)registrationViewControllerDidLogin:(DKRegistrationViewController *)registrationViewController;

@end

@interface DKRegistrationViewController : UIViewController <DKKeyboardHandlerDelegate, UITextFieldDelegate>

@property (weak, nonatomic) id <DonkyRegistrationDelegate> delegate;

- (void)showLoadingView;
- (void)hideLoadingView;

@end