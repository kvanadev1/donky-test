//
//  AppDelegate.h
//  DonkeyExample3
//
//  Created by Kvana Inc 1 on 24/03/14.
//  Copyright (c) 2014 Kvana Inc 1. All rights reserved.
//


#import <UIKit/UIKit.h>
#import "DKRegistrationViewController.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate, DonkyRegistrationDelegate>

@property (strong, nonatomic) UIWindow *window;
@end
