//
//  DKKeyboardHandler.h
//  Donky Client
//
//  Created by Marcin Swierczek on 26/01/2014.
//  Copyright (c) 2014 Dynmark International. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol DKKeyboardHandlerDelegate

- (void)keyboardSizeChanged:(CGSize)delta;

@end

/*
 This is class to handle keyboard events for registartion screen and animate views when keyboard is shown or hiding.
 */
@interface DKKeyboardHandler : NSObject

- (id)init;

@property(nonatomic, assign) id<DKKeyboardHandlerDelegate> delegate;
@property(nonatomic) CGRect frame;

@end