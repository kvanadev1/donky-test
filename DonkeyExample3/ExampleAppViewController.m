//
//  com_dynmark_donky_demoViewController.m
//  Donky
//
//  Created by Technical on 27/01/2014.
//  Copyright (c) 2014 Dynmark International Limited. All rights reserved.
//

#import "ExampleAppViewController.h"
#import "DKDonkyUI.h"
#import "DKDonkyClient.h"

@interface ExampleAppViewController ()
@property (weak, nonatomic) IBOutlet UIButton *inboxButton;
@property (weak, nonatomic) IBOutlet UIButton *chatsButton;
@property (weak, nonatomic) IBOutlet UIButton *contactsButton;

@end

@implementation ExampleAppViewController

@synthesize inboxButton = _inboxButton;
@synthesize chatsButton = _chatsButton;
@synthesize contactsButton = _contactsButton;

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

/**
 Called when 'register' button is clicked
 */
- (IBAction)buttonClicked:(id)sender {
    
    if ([sender tag] == 1) {
        
        // Display  rich messages inbox
        
        UIViewController *donkyRichMessagesViewController = [[DKDonkyUI sharedInstance].uiLauncher donkyClientViewControllerForOption:DKDonkyClientViewControllerOptionRichMessageList];
        UITabBarController *tabControllerRich = (UITabBarController *)donkyRichMessagesViewController;
        
        for (UINavigationController *navController in tabControllerRich.viewControllers) {
            UIViewController *rootViewController = navController.viewControllers[0];
            rootViewController.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Close" style:UIBarButtonItemStyleBordered target:self action:@selector(closeButtonTapped:)];
        }
        [self presentViewController:donkyRichMessagesViewController animated:YES completion:nil];
    }
    else if ([sender tag] == 2) {
        
        // Display chat conversations list
        
        UIViewController *donkyChatMessagesViewController  = [[DKDonkyUI sharedInstance].uiLauncher donkyClientViewControllerForOption:DKDonkyClientViewControllerOptionChatInbox];
        UITabBarController *tabController = (UITabBarController *)donkyChatMessagesViewController;
        
        for (UINavigationController *navController in tabController.viewControllers) {
            UIViewController *rootViewController = navController.viewControllers[0];
            rootViewController.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Close" style:UIBarButtonItemStyleBordered target:self action:@selector(closeButtonTapped:)];
        }
        [self presentViewController:donkyChatMessagesViewController animated:YES completion:nil];
    }
    else if ([sender tag] == 3) {
        
        // Display contacts
        
        UIViewController *donkyContactsViewController  = [[DKDonkyUI sharedInstance].uiLauncher donkyClientViewControllerForOption:DKDonkyClientViewControllerOptionContactList];
        UITabBarController *tabController = (UITabBarController *)donkyContactsViewController;
        
        for (UINavigationController *navController in tabController.viewControllers) {
            UIViewController *rootViewController = navController.viewControllers[0];
            rootViewController.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Close" style:UIBarButtonItemStyleBordered target:self action:@selector(closeButtonTapped:)];
        }
        [self presentViewController:donkyContactsViewController animated:YES completion:nil];
    }
}

/**
 Called when navigation bar close button is tepped in Donky UI view controller
 */
-(void)closeButtonTapped:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

- (BOOL)shouldAutorotate {
    return YES;
}

@end
