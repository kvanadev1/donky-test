//
//  com_dynmark_donky_demoViewController.h
//  Donky
//
//  Created by Technical on 27/01/2014.
//  Copyright (c) 2014 Dynmark International Limited. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ExampleAppViewController : UIViewController
-(void)closeButtonTapped:(id)sender;
@end
