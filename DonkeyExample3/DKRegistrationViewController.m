//
//  DKRegistrationViewController.m
//  Donky Client
//
//  Created by Marcin Swierczek on 26/01/2014.
//  Copyright (c) 2014 Dynmark International. All rights reserved.
//

#import "DKRegistrationViewController.h"
#import <QuartzCore/QuartzCore.h>

#import "ExampleAppViewController.h"
#import "DKUser.h"
#import "DKDonkyClient.h"

@interface DKRegistrationViewController ()

@property (strong, nonatomic) DKKeyboardHandler *keyboard;

@property (weak, nonatomic) IBOutlet UIImageView *donkyImage;
@property (weak, nonatomic) IBOutlet UITextField *firstNameTextField;
@property (weak, nonatomic) IBOutlet UITextField *lastNameTextField;
@property (weak, nonatomic) IBOutlet UITextField *emailTextField;
@property (weak, nonatomic) IBOutlet UITextField *mobileTextField;
@property (weak, nonatomic) IBOutlet UITextField *codeTextField;
@property (weak, nonatomic) IBOutlet UIButton *registerButton;
@property (weak, nonatomic) IBOutlet UILabel *textWelcome;
@property (weak, nonatomic) IBOutlet UILabel *textWelcomeExplain;

@property (weak, nonatomic) IBOutlet UIImageView *loadingSpinningImage;
@property (weak, nonatomic) IBOutlet UIView *waitView;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

@property (weak, nonatomic) UITextField *activeField;

@end

@implementation DKRegistrationViewController

@synthesize keyboard = _keyboard;

@synthesize firstNameTextField = _firstNameTextField;
@synthesize lastNameTextField = _lastNameTextField;
@synthesize emailTextField = _emailTextField;
@synthesize mobileTextField = _mobileTextField;
@synthesize codeTextField = _codeTextField;
@synthesize registerButton = _registerButton;

@synthesize loadingSpinningImage = _loadingSpinningImage;
@synthesize waitView = _waitView;

@synthesize scrollView = _scrollView;
@synthesize activeField = _activeField;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.scrollView.contentSize = CGSizeMake(self.view.bounds.size.width+500, self.view.bounds.size.height+500);
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	[self applyCustomStyleToTextField:self.firstNameTextField];
    [self applyCustomStyleToTextField:self.lastNameTextField];
    [self applyCustomStyleToTextField:self.emailTextField];
    [self applyCustomStyleToTextField:self.mobileTextField];
    [self applyCustomStyleToTextField:self.codeTextField];
    
    self.firstNameTextField.delegate = self;
    self.lastNameTextField.delegate = self;
    self.emailTextField.delegate = self;
    self.mobileTextField.delegate = self;
    self.codeTextField.delegate = self;
    
    self.keyboard = [[DKKeyboardHandler alloc] init];
    self.keyboard.delegate = self;
    
    [self runSpinAnimationOnView:self.loadingSpinningImage];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    self.keyboard.delegate = nil;
    self.keyboard = nil;
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)applyCustomStyleToTextField:(UITextField *)field
{
    field.borderStyle=UITextBorderStyleNone;
    field.layer.masksToBounds=YES;
    field.layer.backgroundColor=[[UIColor colorWithRed:234.0/255.0 green:234.0/255.0 blue:234.0/255.0 alpha:1.0]CGColor];
    field.layer.borderColor=[[UIColor colorWithRed:204.0/255.0 green:204.0/255.0 blue:204.0/255.0 alpha:1.0]CGColor];
    field.layer.borderWidth= 1.0f;
    field.textColor = [UIColor colorWithRed:68.0/255.0 green:68.0/255.0 blue:68.0/255.0 alpha:1.0];
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
    field.leftView = paddingView;
    field.leftViewMode = UITextFieldViewModeAlways;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation {
    if (UIInterfaceOrientationIsPortrait(toInterfaceOrientation)) {
        return YES;
    }
    return NO;
}

- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

- (BOOL)shouldAutorotate {
    return YES;
}

- (IBAction)registerButtonTapped:(id)sender
{

    [self.firstNameTextField resignFirstResponder];
    [self.lastNameTextField resignFirstResponder];
    [self.emailTextField resignFirstResponder];
    [self.mobileTextField resignFirstResponder];
    [self.codeTextField resignFirstResponder];
    
    if (self.firstNameTextField.text.length == 0 || self.firstNameTextField.text == nil) {
        [[[UIAlertView alloc] initWithTitle:@"Error" message:@"First Name missing" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
        return;
    }
    if (self.lastNameTextField.text.length == 0 || self.lastNameTextField.text == nil) {
        [[[UIAlertView alloc] initWithTitle:@"Error" message:@"Last Name missing" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
        return;
    }
    if (self.emailTextField.text.length == 0 || self.emailTextField.text == nil) {
        [[[UIAlertView alloc] initWithTitle:@"Error" message:@"Email missing" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
        return;
    }
    if (self.mobileTextField.text.length == 0 || self.mobileTextField.text == nil) {
        [[[UIAlertView alloc] initWithTitle:@"Error" message:@"Mobile  number missing" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
        return;
    }
    if (self.codeTextField.text.length == 0 || self.codeTextField.text == nil) {
        [[[UIAlertView alloc] initWithTitle:@"Error" message:@"Country Iso Code missing" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
        return;
    }
    
    [self showLoadingView];
    
    // Prepere registartion user data
    
    NSString *firstNameTrimmed = [self.firstNameTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].lowercaseString;
    NSString *lastNameTrimmed = [self.lastNameTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].lowercaseString;

    // In our example external user id is created from user name and surname
    NSString *displayName = [NSString stringWithFormat:@"%@ %@", self.firstNameTextField.text,self.lastNameTextField.text];
    // In our example external user id is created from user name and surname and underbar between them
    NSString *externalUserId = [NSString stringWithFormat:@"%@_%@", firstNameTrimmed,lastNameTrimmed];
    
    // Call user registration method in Donky SDK
    [[DKDonkyClient sharedInstance] registerWithExternalUserId:externalUserId
                                                   displayName:displayName
                                                   countryCode:self.codeTextField.text
                                                  emailAddress:self.emailTextField.text.lowercaseString
                                                   phoneNumber:self.mobileTextField.text
                                          additionalProperties:nil completion:^(NSError *error) {
                                              [self hideLoadingView];
                                              if (error != nil) {
                                                  [self hideLoadingView];
                                                  [[[UIAlertView alloc] initWithTitle:@"Error" message:[error localizedDescription] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
                                              } else {
                                                  // User registered successfuly
                                                  [self.delegate registrationViewControllerDidRegister:self];
                                              }
                                          }];

    
}

#pragma mark - displaying progress view

/**
 Show view with progress spinner
 */
- (void)showLoadingView
{
    NSLog(@"showLoadingView in");
    [UIView animateWithDuration:3.f delay:0.f options:UIViewAnimationOptionCurveEaseIn animations:^{
        [self.waitView setAlpha:1.f];
    } completion:^(BOOL finished) {

    }];
}

/**
 Hide view with progress spinner
 */
- (void)hideLoadingView
{
    [UIView animateWithDuration:0.5f delay:0.f options:UIViewAnimationOptionCurveEaseIn animations:^{
         [self.waitView setAlpha:0.f];
    } completion:^(BOOL finished) {

    }];
}

/**
 Start progress spinner animation
 */
- (void) runSpinAnimationOnView:(UIView*)view;
{
    CABasicAnimation* rotationAnimation;
    CGFloat duration = 1e10f;
    rotationAnimation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
    rotationAnimation.toValue = [NSNumber numberWithFloat: M_PI * 2.0 /* full rotation*/ * duration];
    rotationAnimation.duration = duration;
    rotationAnimation.cumulative = YES;
    rotationAnimation.repeatCount = 1.0;
    
    [view.layer addAnimation:rotationAnimation forKey:@"rotationAnimation"];
}

#pragma mark - handling keyboard hiding/showing

- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    
}

/**
 adjust scrollview and controller view when keybord is shown
 */
- (void)keyboardWasShown:(NSNotification*)aNotification
{
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height, 0.0);
    self.scrollView.contentInset = contentInsets;
    self.scrollView.scrollIndicatorInsets = contentInsets;
    
    // If active text field is hidden by keyboard, scroll it so it's visible
    CGRect aRect = self.view.frame;
    aRect.size.height -= kbSize.height;
    if (!CGRectContainsPoint(aRect, self.activeField.frame.origin) ) {
        [self.scrollView scrollRectToVisible:self.activeField.frame animated:YES];
    }
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    self.activeField = textField;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    self.activeField = nil;
}

/**
 adjust scrollview when keybord is hidden
 */
- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    self.scrollView.contentInset = contentInsets;
    self.scrollView.scrollIndicatorInsets = contentInsets;
}

/**
 adjust controller view when keybord size changed
 */
- (void)keyboardSizeChanged:(CGSize)delta
{
    CGRect frame = self.view.frame;
    frame.size.height -= delta.height;
    self.view.frame = frame;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

@end
