//
//  DKDonkyClientViewController.h
//  DonkySDK
//
//  Created by David Taylor on 02/12/2013.
//  Copyright (c) 2013 Compsoft. All rights reserved.
//

#import <UIKit/UIKit.h>

 /*! 
 @abstract Houses the complete Donky experience
 */
@interface DKDonkyClientViewController : UITabBarController

@end
