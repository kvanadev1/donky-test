//
//  DKRichMessageController.h
//  DonkySDK
//
//  Created by David Taylor on 25/11/2013.
//  Copyright (c) 2013 Compsoft. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "DKConstants.h"

@class DKRichMessageController, DKRichMessage, DKConversationThread;

typedef void (^DKRichMessageConversationThreadCreated) (DKConversationThread *conversationThread);
typedef void (^DKRichMessageForwardCompletion) (NSError *error);

/*! 
 @abstract Informs delegates of new and updated rich message
 */
@protocol DKRichMessageControllerDelegate <NSObject>

/*!  
 @abstract Passes new and updated rich messages to the delegates
 
 @param addedRichMessages   New rich messages
 @param updatedRichMessages Updated rich messages
 @param removedRichMessages Removed rich messages
 */
- (void)richMessagesAdded:(NSArray *)addedRichMessages updated:(NSArray *)updatedRichMessages;

@end

/*! 
 @abstract Responsible for the management of rich messages
 */
@interface DKRichMessageController : NSObject

/*!  
 @abstract Retrieves an array of rich messages filtered by their descriptions
 
 @param filter  The string to filter by
 
 @return An array of DKRichMessages
 */
- (NSArray *)allRichMessages:(NSString *)filter;

/*!  
 @abstract Retrieves an array of rich messages filtered by their descriptions
 
 @param filter  The string to filter by
 @param offset The offset of the query
 @param limit  The maximum number of conversation threads returned
 
 @return An array of DKRichMessages
 */
- (NSArray *)allRichMessages:(NSString *)filter offset:(NSUInteger)offset limit:(NSUInteger)limit;

/*!  
 @return Returns the number of unexpired rich messages that have been saved to the device
 */
- (NSInteger)richMessageCount;

/*!  
 @return Returns the number of unexpired and unread rich messages that have been saved to the device
 */
- (NSInteger)richMessageUnreadCount;

/*!  
 @abstract Replys to a rich message
 
 @param richMessageId                The id of the rich message to reply to
 @param messageText                  The reply message
 @param attachments                  An array of reply attachments
 @param completion                   Called when the message has been sent or on error
 
 @return The new conversation thread
 */
- (DKConversationThread *)replyToRichMessage:(NSString *)richMessageId messageText:(NSString *)messageText assetIds:(NSArray *)assetIds completion:(DKCompletionBlock)completion;

/*!  
 @abstract Forwards a rich message to a contact
 
 @discussion When the conversation thread has been saved to the database the messageSent block gets called
 
 @param richMessageId    The id of the rich message to reply to
 @param userIds          The external ids of the users the message is being forwarded to
 @param message          The forward message limited to 160 characters
 @param completion       A block called when the rich message has been sent
 */
- (void)forwardRichMessage:(NSString *)richMessageId toUsers:(NSArray *)userIds message:(NSString *)message onCompletion:(DKRichMessageForwardCompletion)completion;

/*!  
 @abstract Retrieves the rich message with the given Id
 
 @param richMessageId The rich message to retrieve
 
 @return The rich message with the
 */
- (DKRichMessage *)richMessageById:(NSString *)messageId;

/*!  
 @abstract Deletes the rich message with the given Id
 
 @param messageId The rich message to delete
 @param error     On error deleting the message
 */
- (void)deleteRichMessageWithId:(NSString *)messageId error:(NSError **)error;

/*!  
 @abstract Marks a rich message as read
 
 @param messageId The rich message to mark as read
 */
- (void)markRichMessageRead:(DKRichMessage *)richMessage;

/*!  
 @abstract Adds a delegate
 
 @discussion Any added delegate should conform to DKChatControllerDelegate protocol. Delegates are notified using notifyDelegatesOfChatMessagesAdded:andUpdated:. Delegates that are nilled off are automatically removed.
 
 @param delegate    An object that conforms to DKRichMessageControllerDelegate
 */
- (void)addDelegate:(id<DKRichMessageControllerDelegate>)delegate;

/*!  
 @abstract Removes a delegate
 
 @param delegate    An object that conforms to DKRichMessageControllerDelegate
 */
- (void)removeDelegate:(id<DKRichMessageControllerDelegate>)delegate;

@end
