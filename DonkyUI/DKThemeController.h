//
//  DKThemeController.h
//  DonkySDK
//
//  Created by David Taylor on 02/12/2013.
//  Copyright (c) 2013 Compsoft. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DKTheme.h"

/*! 
 @abstract Controls which theme the UI displays
 */
@interface DKThemeController : NSObject

/*! 
 @abstract The theme to display
 
 @discussion Defaults to the Donky purple theme. To display a different theme set this before presenting any of the UI [DKDonkyUI sharedInstance].themeController.selectedTheme = [DKTheme glossyBlackTheme];
 */
@property (strong, nonatomic) DKTheme *selectedTheme;

@end
