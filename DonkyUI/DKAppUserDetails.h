//
//  DKAppUserDetails.h
//  DonkySDK
//
//  Created by David Taylor on 27/02/2014.
//  Copyright (c) 2014 Compsoft. All rights reserved.
//

#import <Foundation/Foundation.h>

@class DKUser;

typedef enum {
    DKStatusSuccess,
    DKStatusNotRegistered,
} DKStatus;

@interface DKAppUserDetails : NSObject

- (DKStatus)status;
- (DKUser *)user;

@end
