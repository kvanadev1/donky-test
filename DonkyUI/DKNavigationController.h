//
//  DKNavigationController.h
//  DonkySDK
//
//  Created by David Taylor on 02/12/2013.
//  Copyright (c) 2013 Compsoft. All rights reserved.
//

#import <UIKit/UIKit.h>

@class DKTheme;

@interface DKNavigationController : UINavigationController

@property (strong, nonatomic) DKTheme *theme;

/*! 
 @abstract Controls whether the top view controller should be popped off the navigation controller when the back bar button is tapped
 
 @discussion If set to YES, prevents the current pushed view controller from being popped off the screen
 */
@property (nonatomic) BOOL shouldPreventDefaultBackButtonAction;

/*! 
 @abstract Sets what message to display for the pop up message when preventing the view controller from popping off the screen
 */
@property (strong, nonatomic) NSString *preventDefaultBackButtonAlertViewMessage;

@end
