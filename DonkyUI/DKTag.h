//
//  DKTag.h
//  DonkySDK
//
//  Created by David Taylor on 04/12/2013.
//  Copyright (c) 2013 Compsoft. All rights reserved.
//

#import <Foundation/Foundation.h>

/*! 
 @abstract A class that represents a tag
 */
@interface DKTag : NSObject

/*! 
 @abstract The name of the tag
 */
@property (nonatomic, retain) NSString * name;

/*! 
 @abstract Whether the tag is selected or not
 */
@property (nonatomic) BOOL selected;

@end
