//
//  DKChatController.h
//  DonkySDK
//
//  Created by Tristan Warner-Smith on 19/12/2013.
//  Copyright (c) 2013 Compsoft. All rights reserved.
//

#import "DKConstants.h"
#import "DKConversationThread.h"

@class DKChatController, DKNotificationChangeSet;

typedef enum {
    DKChatErrorCodeMessageRateMaxExceeded = 26,
} DKChatErrorCodes;

typedef void (^DConversationThreadCreateCompletionBlock) (DKConversationThread *conversationThread, NSError *error);

/*! 
 @abstract Informs delegates of new and updated chat messages
 */
@protocol DKChatControllerDelegate <NSObject>

/*!  
 @abstract Passes new and updated chat messages to the delegates
 
 @param addedChatMessages        New chat messages
 @param updatedChatMessages      Updated chat messages
 @param addedGroupChatMessages   New group chat messages
 @param updatedGroupChatMessages Updated group chat messages
 */
- (void)chatMessagesAdded:(NSArray *)addedChatMessages chatMessagesUpdated:(NSArray *)updatedChatMessages groupChatMessagesAdded:(NSArray *)addedGroupChatMessages andGroupChatMessagesUpdated:(NSArray *)updatedGroupChatMessages;

@end

/*! 
 @abstract Responseible for interacting with the messaging service provided by Donky Messaging
 */
@interface DKChatController : NSObject

/*!  
 @abstract Retrives an array of conversation threads filtered by their last chat message
 
 @param filter The text you want to filter the messages by
 
 @return An array of DKConversationThreads
 */
- (NSArray *)allConversationThreads:(NSString *)filter;

/*!  
 @abstract Retrives an array of conversation threads filtered by their last chat message
 
 @param filter The text you want to filter the messages by
 @param offset The offset of the query
 @param limit  The maximum number of conversation threads returned
 
 @return An array of DKConversationThreads
 */
- (NSArray *)allConversationThreads:(NSString *)filter offset:(NSUInteger)offset limit:(NSUInteger)limit;

/*!  
 @abstract Retrieves a conversation thread
 
 @param conversationId The id of the conversation thread
 
 @return A conversation thread
 */
- (DKConversationThread *)conversationById:(NSString *)conversationId;

/*!  
 @abstract Retrieves the single chat conversation thread for a given contact. If no such conversation thread exists, one it created.
 
 @param contactId The externalUserId of the contact
 @param error On error creating conversation
 
 @return A conversation thread with all the single chat messages associated with the contact
 */
- (DKConversationThread *)createConversationForContactWithId:(NSString *)contactId error:(NSError **)error;

/*!  
 @abstract Creates a group conversation with the specified contacts
 
 @param contactIds The externalUserIds of the contacts
 @param error On error creating conversation
 
 @return A conversation thread with all the group chat messages associated with the contacts
 */
- (DKConversationThread *)createConversationForContactsWithIds:(NSArray *)contactIds error:(NSError **)error;

/*!  
 @abstract Retrieves the conversation thread for a rich message
 
 @param richMessageId    The id of the rich message which the conversation thread is based off of
 
 @return A new conversation thread
 */
- (DKConversationThread *)conversationThreadForRichMessageWithId:(NSString *)richMessageId;

/*!  
 @abstract Returns a list of chat messages
 
 @param type            DKSingleChat or DKGroupChat
 @param conversationId  Conversation that the chat messages should belong to
 @param offset          The offset of the query
 @param limit           The maximum number of returned chat messages
 
 @return An array of DKChatMessages
 */
- (NSArray *)chatMessagesForType:(DKChatType)type andConversationId:(NSString *)conversationId offset:(NSUInteger)offset limit:(NSUInteger)limit;

/*!  
 @abstract Returns the count of chat messages for a given conversation thread
 
 @param type           DKSingleChat or DKGroupChat
 @param conversationId conversation that the chat messages should belong to
 
 @return The number of chat messages in the convrsation thread
 */
- (NSUInteger)chatMessageCountForType:(DKChatType)type andConversationId:(NSString *)conversationId;

/*!  
 @return The total number of read chat messages
 */
- (NSInteger)chatMessageCount;

/*!  
 @return The total number of unread chat messages
 */
- (NSInteger)chatMessageUnreadCount;

/*!  
 @abstract Marks a given conversation thread as read for all unread messages in that thread triggering the appropriate message read client notifications
 
 @param thread          The conversation thread
 @param completionBlock The codeblock to run when the message has been updated
 */
- (void)markConversationAsRead:(DKConversationThread *)thread withCompletion:(DKCompletionBlock)completionBlock;

/*!  
 @abstract Deletes a conversation thread and all its associate chat messages from the database
 
 @param conversationId The Id of the conversation thread to be delete
 @param completion     Group chats must inform Donky Messaging Nextwork that the user has deleted the conversation and thus left the group.
 */
- (void)deleteConversationThreadWithId:(NSString *)conversationId completion:(DKCompletionBlock)completion;

/*!  
 @abstract Deletes a conversation thread and all its associate chat messages from the database
 
 @param conversationId The Id of the conversation thread to be delete
 @param error          On error deleting the conversation thread
 */
- (void)deleteConversationThreadWithId:(NSString *)conversationId error:(NSError **)error;

/*!  
 @abstract Saves a message and a list of assets against a conversation thread that can be retrieved later
 
 @param message              The message to save, gets set to nil ager sending the message
 @param assetIds             The Ids of the assets to save, gets set to nil ager sending the message
 @param conversationThreadId The Id of the conversation thread to save against
 @param error                On error saving the message
 */
- (void)saveMessage:(NSString *)message assets:(NSArray *)assetIds forConversationThreadWithId:(NSString *)conversationThreadId error:(NSError **)error;

/*!  
 @abstract Sends a chat message to a specified contact
 
 @discussion Creates a new conversation thread
 
 @param message                            The text to be sent
 @param assetIds                           The asset ids to attach
 @param externalUserIds                    The externalUserId of the contact for the message to be sent to
 
 @return The id of the new conversation thread
 */
- (DKConversationThread *)sendMessage:(NSString *)message withAssetIds:(NSArray *)assetIds toContactWithExternalUserIds:(NSArray *)externalUserIds withCompletion:(DKCompletionBlock)completion;

/*!  
 @abstract Sends a chat message to an existing conversation thread
 
 @param message         The text to be sent
 @param assetIds        The asset ids to attach
 @param converstationId The id of the converstation thread the message is being sent to
 */
- (void)sendMessage:(NSString *)message withAssetIds:(NSArray *)assetIds toConversationThread:(NSString *)converstationId withCompletion:(DKCompletionBlock)completion;

/*!  
 @abstract Sends a chat message to an existing conversation thread while adding extra participants to the group
 
 @param message         The message to be sent
 @param converstationId The id of the conversation thread the message is being sent to
 @param externalUserIds An array of externalUserIds to add to the conversation
 */
- (void)sendMessage:(NSString *)message withAssetIds:(NSArray *)assetIds toConversationThread:(NSString *)converstationId addContactsExternalUserIds:(NSArray *)externalUserIds withCompletion:(DKCompletionBlock)completion;

/*!  
 @abstract Adds the specified participants to the specified group chat.
 
 @param contactIds     The recipients to add to the group chat
 @param conversationId The group conversation identifier
 @param completion     Called when the 
 */
- (void)addContacts:(NSArray *)contactIds toGroupWithConversationId:(NSString *)conversationId completion:(DKCompletionBlock)completion;

/*!  
 @abstract Removes the user from a group conversation
 
 @param conversationId The conversation identifier
 */
- (void)leaveGroupWithConverationId:(NSString *)conversationId completion:(DKCompletionBlock)completion;

#pragma mark - Delegates

/*!  
 @abstract Adds a delegate
 
 @discussion Any added delegate should conform to DKChatControllerDelegate protocol. Delegates are notified using notifyDelegatesOfChatMessagesAdded:andUpdated:. Delegates that are nilled off are automatically removed
 
 @param delegate An object that conforms to DKChatControllerDelegate
 */
- (void)addDelegate:(id<DKChatControllerDelegate>)delegate;

/*!  
 @abstract Removes a delegate
 
 @param delegate An object that conforms to DKChatControllerDelegate
 */
- (void)removeDelegate:(id<DKChatControllerDelegate>)delegate;

@end