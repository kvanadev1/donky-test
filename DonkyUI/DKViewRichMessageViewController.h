//
//  DKViewRichMessageViewController.h
//  DonkySDK
//
//  Created by David Taylor on 28/11/2013.
//  Copyright (c) 2013 Compsoft. All rights reserved.
//

#import "DKBaseViewController.h"

@class DKViewRichMessageViewController, DKRichMessage, DKConversationThread;

/*! 
 @abstract Events raised by DKViewRichMessageViewController
 */
@protocol DKViewRichMessageViewControllerDelegate <NSObject>

@optional
/*!  
 @abstract Called when the user taps reply on a forwarded message
 
 @param viewRichMessageViewController The DKViewRichMessageViewController that received the tap
 @param conversationThread            The conversation thread belonging to the contact who forwarded the rich message
 */
- (void)viewRichMessageViewController:(DKViewRichMessageViewController *)viewRichMessageViewController didTapReplyToForwardContact:(DKConversationThread *)conversationThread;

/*!  
 @abstract Called when the user taps send on a rich message
 
 @param viewRichMessageViewController The DKViewRichMessageViewController that the call originated from
 @param conversationThread            The conversation thread belonging to the contact who sent the rich message
 */
- (void)viewRichMessageViewController:(DKViewRichMessageViewController *)viewRichMessageViewController didSendMessageToRichMessageContact:(DKConversationThread *)conversationThread;

/*!  
 @abstract Called when the user taps a link in the rich message
 
 @param viewRichMessageViewController The DKViewRichMessageViewController that received the tap
 @param link                          The uRL of the tapped link
 */
- (void)viewRichMessageViewController:(DKViewRichMessageViewController *)viewRichMessageViewController didTapLink:(NSURL *)link;

@end

/*! 
 @abstract Displays a rich message and any forwarded messages associated with it
 */
@interface DKViewRichMessageViewController : DKBaseViewController <UIWebViewDelegate>

/*! 
 @abstract Overrides events raised by DKRichMessageListViewController
 */
@property (weak, nonatomic) id <DKViewRichMessageViewControllerDelegate> delegate;

/*! 
 @abstract The rich message that is being displayed by this controller
 */
@property (strong, nonatomic) DKRichMessage *richMessage;

/*! 
 @abstract Controls whether the rich message is in forward to mode
 
 @discussion If YES then the recipient list view is shown as well as the chat reply view tailored for forwarding messages
 */
@property (nonatomic) BOOL shouldShowForwardDialog;


@end
