//
//  DKContact.h
//  DonkySDK
//
//  Created by David Taylor on 04/12/2013.
//  Copyright (c) 2013 Compsoft. All rights reserved.
//

#import <Foundation/Foundation.h>

@class DKAsset;

/*! 
 @abstract A class that represents a contact
 */
@interface DKContact : NSObject

/*! 
 @abstract The contacts display name
 */
@property (nonatomic, retain) NSString * displayName;

/*! 
 @abstract The contacts email address
 */
@property (nonatomic, retain) NSString * emailAddress;

/*! 
 @abstract The contacts external user Id
 */
@property (nonatomic, retain) NSString * externalUserId;

/*! 
 @abstract Whether the contact is a Donky Messaging standard contact
 */
@property (nonatomic) BOOL isStandard;

/*! 
 @abstract The contacts phone number
 */
@property (nonatomic, retain) NSString * phoneNumber;

/*! 
 @abstract The contacts avatar
 */
@property (nonatomic, retain) DKAsset *avatarAsset;

/*! 
 @abstract An array of DKContactProperty that contains addition properties specified during registration
 */
@property (nonatomic, retain) NSArray *additionalProperties;

@end