//
//  DKBaseTableViewController.h
//  DonkySDK
//
//  Created by David Taylor on 29/11/2013.
//  Copyright (c) 2013 Compsoft. All rights reserved.
//

#import <UIKit/UIKit.h>

@class DKTheme;

@interface DKBaseTableViewController : UITableViewController <UIAlertViewDelegate, UISearchDisplayDelegate>

@end