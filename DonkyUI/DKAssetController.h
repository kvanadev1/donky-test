//
//  DKAssetController.h
//  DonkySDK
//
//  Created by David Taylor on 03/12/2013.
//  Copyright (c) 2013 Compsoft. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "DKConstants.h"

@class UIImage, DKAsset;

/*!
 A callback after downloading an asset from the service
 
 @param assetId       The id of the asset
 @param asset         The asset image
 @param assetFilePath The assets filepath
 @param error         On error of downloading the asset. Error codes: DKAssetControllerErrorInvalidAssetId
 */
typedef void (^DKAssetDownloadCompletion) (NSString *assetId, UIImage *asset, NSString *assetFilePath, NSError *error);

/*!
 A callback after uploading an asset from the service
 
 @param asset The newly created asset
 @param error On error of downloading the asset. Error codes: DKAssetControllerErrorCodeFileNotFound
 */
typedef void (^DKAssetUploadCompletion) (DKAsset *asset, NSError *error);

/*! 
 @abstract Responsible for the upload and download of assets
 */
@interface DKAssetController : NSObject

/*! 
 @abstract Get the asset by properties informing on completion
 
 @param assetId       The Id of the Asset to get
 @param imageMimeType The mime type of the image to get
 @param completion    The code to execute on completion
 */
- (void)getAssetImageById:(NSString *)assetId ofImageMimeType:(NSString *)imageMimeType completion:(DKAssetDownloadCompletion)completion;

/*!  
 @abstract Uploads an asset to the service
 
 @param filePath   File path for the image to be uploaded
 @param completion A callback that holds the uploaded asset information and any errors
 */
- (void)uploadAssetPath:(NSString *)filePath completion:(DKAssetUploadCompletion)completion;

@end
