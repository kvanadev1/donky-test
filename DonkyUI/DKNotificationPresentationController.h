//
//  DKNotificationPresentationController.h
//  DonkySDK
//
//  Created by David Taylor on 28/01/2014.
//  Copyright (c) 2014 Compsoft. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

/*! 
 @abstract Handles the displaying of local notifications created during the notification exchange
 */
@interface DKNotificationPresentationController : NSObject

/*!  
 @abstract Displays the banner at the top of the screen
 
 @param localNotification The notification to display
 */
- (void)presentLocalNotification:(UILocalNotification *)localNotification;

/*!  
 @abstract Navigates the app to an instance of DKChatViewController populated with the message
 
 @param remoteNotification The remote notification data
 */
- (void)handleRemoteNotification:(NSDictionary *)remoteNotification;

@end
