//
//  DKSettingsViewController.h
//  DonkySDK
//
//  Created by David Taylor on 10/12/2013.
//  Copyright (c) 2013 Compsoft. All rights reserved.
//

#import "DKBaseTableViewController.h"
#import <MessageUI/MessageUI.h>

@class DKSettingsViewController;

@protocol DKSettingsViewControllerDelegate <NSObject>

- (void)settingsViewController:(DKSettingsViewController *)settingsViewController willDisplayViewController:(UIViewController *)viewController;

@end

/*! 
 @abstract Displays a list of configurable settings
 */
@interface DKSettingsViewController : DKBaseTableViewController <UINavigationControllerDelegate, UIImagePickerControllerDelegate, UIActionSheetDelegate, MFMessageComposeViewControllerDelegate, MFMailComposeViewControllerDelegate>

@property (weak, nonatomic) id <DKSettingsViewControllerDelegate> delegate;

- (void)selectInitialSettingsOption;

@end
