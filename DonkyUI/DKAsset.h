//
//  DKAsset.h
//  DonkySDK
//
//  Created by David Taylor on 04/12/2013.
//  Copyright (c) 2013 Compsoft. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSString * const DKMimeTypePNG;
extern NSString * const DKMimeTypeJPG;

@class DKRichMessage;

/*! 
 @abstract A class that represents an attachment or avatar
 */
@interface DKAsset : NSObject

/*! 
 @abstract Unique Id for the asset
 */
@property (nonatomic, retain) NSString *assetId;

/*! 
 @abstract The asset mimetype e.g. image/png
 */
@property (nonatomic, retain) NSString * mimeType;

/*! 
 @abstract The file path of the asset
 */
@property (nonatomic, retain) NSString * filePath;

@end
